#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/lddev001/VS_ws/idc_ibeo6/build/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:./:./tools"
export ROSLISP_PACKAGE_DIRECTORIES="/home/lddev001/VS_ws/idc_ibeo6/build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/lddev001/VS_ws/idc_ibeo6:$ROS_PACKAGE_PATH"