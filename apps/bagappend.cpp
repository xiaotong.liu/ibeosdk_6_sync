/*!
@file bagappend.cpp
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief add up all can bags in folder
*/
#include "bagappend.h"
#include <boost/foreach.hpp>
#include "filemanager.h"
#define foreach BOOST_FOREACH

bagAppend::bagAppend() {}
std::vector<ld_msgs::ld_can> bagAppend::canbag_app(const std::string& filepath) {
  FileManager fileHandler;
  std::vector<std::string> filenames;
  filenames = fileHandler.GetAllBagFiles(filepath, ".bag");
  int NbCan;
  NbCan = filenames.size();
  std::vector<ld_msgs::ld_can> can_all;
  //  rosbag::Bag can_all;
  //  can_all.open("canInTotal.bag", rosbag::BagMode::Write);
  ld_msgs::ld_can::ConstPtr S;
  // open every sub can_bag
  for (int i = 0; i < NbCan; i++) {
    rosbag::Bag temp_bag;
    temp_bag.open(filenames.at(i), rosbag::BagMode::Read);
    std::vector<std::string> topics;
    topics.push_back(std::string("/ld_can"));
    rosbag::View view(temp_bag, rosbag::TopicQuery(topics));
    foreach (rosbag::MessageInstance const m,
             view) {  // A class pointing into a bag file.
      ld_msgs::ld_can::ConstPtr s = m.instantiate<ld_msgs::ld_can>();
      can_all.push_back(*s);
      //      S = s;
      //      can_all.write("/ld_can", s->header.stamp, *s);
      //      std::cout << "can msg time stamp is " << *S << std::endl;
    }
    temp_bag.close();
  }
  //    can_all.close();
  return can_all;
}
