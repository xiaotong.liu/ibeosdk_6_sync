/*!
@file bagsync.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief sync bags
*/
#ifndef BAGSYNC_H
#define BAGSYNC_H
// libs for rosbag API
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <string>
#include <vector>
#include "std_msgs/String.h"

class bagSync {
 public:
  bagSync();
  void write_sync(std::string pc_bag,
                  std::vector<sensor_msgs::PointCloud2>& pc_value, std::vector<ros::Time>& pc_time,
                  std::vector<ld_msgs::ld_imugps_can>& iCan, int count,
                  std::string output_path, std::string topic);
};

#endif  // BAGSYNC_H
