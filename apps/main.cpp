
#include <utility>
#include <iostream>
#include <fstream>
#include <ibeo/common/sdk/lux.hpp>
#include <ibeo/common/sdk/ecu.hpp>
#include "filemanager.h"
#include <bagappend.h>
#include <bagsync.h>
#include <caninterpreter.h>
#include <filemanager.h>
#include <interpolationcan.h>
#include <ivparser.h>
#include <timerange.h>
#include "imgParser.h"
#include "ibeo/common/sdk/datablocks/frameindex/special/FrameIndex6130.hpp"
#include "ibeo/common/sdk/devices/IdcFile.hpp"
#include "ibeo/common/sdk/datablocks/objectlist/special/ObjectList2281.hpp"
#include <ibeo/common/sdk/listener/DataContainerListener.hpp>
// #include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"
#include "ivpcl/ld_ivpoint_pcl.h"
#include "ivpcl/ld_ospoint_pcl.h"
#include <ivparser.h>
#include <interpolation.h>
#include <sync_idc.h>
#include <fstream>
#include <math.h>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"

// PCL library
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <autoware_msgs/DetectedObject.h>
#include <autoware_msgs/DetectedObjectArray.h>
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <ld_can/can_interpreter.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include "yamlparser.h"
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include <jmemio.h>
#include <jmem_src.h>
#include <jmem_dest.h>
#include <jpegfix.h>
#include <ld_msgs/ld_image.h>
#include <ld_msgs/ld_object_list.h>

// #ifdef WITHJPEGSUPPORT
// #    include <ibeosdk/jpegsupport/jmemio.h>
// #endif // WITHJPEGSUPPORT

// #ifdef WITHJPEGSUPPORTDEF
// #    include <ibeosdk/jpegsupport/jmemio.h>
// #endif


using IdcFile = ibeo::common::sdk::IdcFile;
using FrameIndex6130 = ibeo::common::sdk::FrameIndex6130;
using IdcFileController = ibeo::common::sdk::IdcFileController;
using FrameIndexEntryIn6130 = ibeo::common::sdk::FrameIndexEntryIn6130;
using DataTypeId = ibeo::common::sdk::DataTypeId;
using IbeoDataHeader = ibeo::common::sdk::IbeoDataHeader;
using NTPTime = ibeo::common::sdk::NTPTime;
using ObjectList2281 = ibeo::common::sdk::ObjectList2281;

std::vector<NTPTime> x_s;
std::vector<NTPTime> can_ts;
std::vector<std::string> pc_s;
std::vector<ros::Time> rs_ts;
std::vector<ros::Time> velo_ts;
std::vector<ros::Time> yaw_ts;
std::vector<pcl::PointCloud <ld::ivStruct> > pcl_pointclouds;
std::vector<ros::Time> tr_scantimestamp;
std::vector<ros::Time> objtr_scantimestamp;
ros::Time rs_time;
ros::Time objrs_time;
std::ofstream can_file;
int flag_2280 = 0;
int flag_2281 = 0;
std::vector<ld_msgs::ld_imugps_can> v_can_velocity;
std::vector<ld_msgs::ld_imugps_can> v_can_yawrate;

// std::vector<autoware_msgs::DetectedObjectArray> ObjectsList;
std::vector<ld_msgs::ld_object_list> ObjectsList;
std::vector<std::string> para_topic;

ld::CanInterpreter<double> velocity;
ld::CanInterpreter<double> yaw;
ld::CanInterpreter<double> accx;
ld::CanInterpreter<double> accy;
std::vector<int> id_list;
std::vector<int> camera_id;

std::vector<ld_msgs::ld_image> img_Fs;
std::vector<ld_msgs::ld_image> img_Bs;
std::vector<ld_msgs::ld_image> img_ros;
std::vector<ros::Time> img_rs_time;
int img_cnt;
std::string img_filepath;


using namespace ibeo::common::sdk;
TimeConversion tc;


double yawrate_calculate(int& yawrate_vz, double& yawrate_without_vz) {
  switch (yawrate_vz) {
    case 0:
      return yawrate_without_vz;
    case 1:
      return -1 * yawrate_without_vz;
  }
}

class AllListener
: public ibeo::common::sdk::DataContainerListener<Scan2205, DataTypeId::DataType_Scan2205>,
  public ibeo::common::sdk::DataContainerListener<ObjectList2280, DataTypeId::DataType_ObjectList2280>,
  public ibeo::common::sdk::DataContainerListener<ObjectList2281, DataTypeId::DataType_ObjectList2281>,
  public ibeo::common::sdk::DataContainerListener<CanMessage1002, DataTypeId::DataType_CanMessage1002>,
  public ibeo::common::sdk::DataContainerListener<Image2403, DataTypeId::DataType_Image2403>{
public:
    virtual ~AllListener() {}

    //========================================
    void onData(const CanMessage1002& canMsg) override
    {
        // LOGTRACE(appLogger, "Received CanMessage1002Container " << static_cast<const void*>(&canMsg));
        // LOGTRACE(appLogger, "========================================================");
        // spdlog::info("CAN found");
        int ID = canMsg.getCanId();
        int IDdec = (int) canMsg.getCanId();

        std::string IDhex = ibeo::common::sdk::toHex(IDdec);
        uint8_t data[8];

        ld_msgs::ld_imugps_can can_write_velocity; //这里只会存速度，每次是覆盖速度之后再存，所以每次存的时候不会带上上一次的旧数据
        ld_msgs::ld_imugps_can can_write_yawrate_acc;

        double velocity_raw;
        double velocity_kmh;
        double velocity_ms;
        double yawrate_raw;
        double yawrate_without_vz;
        int yawrate_vz;
        double yawrate_degree;
        double yawrate_radian;
        double acc_x_raw;
        double acc_x;
        double acc_y_raw;
        double acc_y;
        const double kGravi=9.8;
        const double kPi=3.1415926;

        // // ============= velocity =====================
        // ld::CanInterpreter<double> vel;
        data[0] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte0);
        data[1] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte1);
        data[2] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte2);
        data[3] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte3);
        data[4] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte4);
        data[5] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte5);
        data[6] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte6);
        data[7] = (int)canMsg.getData(ibeo::common::sdk::CanMessage1002::ByteNumber::Byte7);
        int len = *(&data + 1) - data;

        if(ID == id_list.at(0)){

        double temp_time = canMsg.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
        temp_time = temp_time / 1000;
        rs_time.fromSec(temp_time);
        velo_ts.push_back(rs_time);


        
        can_write_velocity.v_x_CAN = velocity.can_parser(data, len);
        can_write_velocity.can_ts = rs_time;
        can_write_velocity.header.frame_id=canMsg.getCanId();

        v_can_velocity.push_back(can_write_velocity);

        }
        else if (ID == id_list.at(1)){
         double temp_time = canMsg.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
         temp_time = temp_time / 1000;
         rs_time.fromSec(temp_time);
         yaw_ts.push_back(rs_time);


        //  yawrate_raw=(data[6]&0x3f)<<8|data[5];
        //  yawrate_without_vz=yawrate_raw*0.01;   // == yaw.can_parse()
         double yawrate_radian = yaw.can_parser(data,len) / 180 * kPi;

        //  double yawrate_radian = yaw.can_parser(data,len) * yaw.get_unit_trans() / 180 * kPi;

        //  yawrate_vz=(data[6]&0x40)>>6;
        //  yawrate_degree=yawrate_calculate(yawrate_vz,yawrate_without_vz);
        //  std::cout << "sign 1 " << yaw.get_sign() << ", sign 2 " << yawrate_degree << std::endl;
        //  std::cout << "sign 2 " << yawrate_degree << std::endl;
        //  yawrate_radian=yawrate_degree/180*kPi;
        //  std::cout << "yawrate_radian is " << yawrate_radian << std::endl;

         can_write_yawrate_acc.can_ts=rs_time;
         can_write_yawrate_acc.header.frame_id=canMsg.getCanId();
         can_write_yawrate_acc.acceleration_x = accx.can_parser(data, len);
         can_write_yawrate_acc.acceleration_y = accy.can_parser(data, len);
         can_write_yawrate_acc.yawrate = yawrate_radian;

         v_can_yawrate.push_back(can_write_yawrate_acc);
        }
    }
    //========================================
    virtual void onData(const Scan2205& scan) override
    {

        double temp_time = scan.getStartTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
        temp_time = temp_time /1000;
        rs_time.fromSec(temp_time);
        tr_scantimestamp.push_back(rs_time);

        //		std::cout << "The time stamp in ROS " << temp_time << std::endl;
        int num = scan.getNumberOfScanPoints();
        pcl::PointCloud<ld::ivStruct> pcl_pointcloud;
        std::vector<ibeo::common::sdk::ScanPointIn2205> vec = scan.getScanPoints();
        // std::cout << "scan flag is " << scan.getFlags() << std::endl;
        pcl_pointcloud.width = num;
        pcl_pointcloud.height = 1;
        pcl_pointcloud.is_dense = false;
        pcl_pointcloud.points.resize(pcl_pointcloud.width * pcl_pointcloud.height);
        //		pcl_pointcloud.header.stamp = rs_time;
    //    std::cout << "point cloud time is   " << rs_time <<std::endl;
        for (int i=0; i<num; i++){
                pcl_pointcloud.points[i].x = vec[i].getPositionX();
                pcl_pointcloud.points[i].y = vec[i].getPositionY();
                pcl_pointcloud.points[i].z = vec[i].getPositionZ();
                pcl_pointcloud.points[i].intensity = vec[i].getIntensity();
                // pcl_pointcloud.points[i].flags = vec[i].getFlags();
        }
        pcl_pointclouds.push_back(pcl_pointcloud);
    }
    // ========================================

    //========================================
    virtual void onData(const ObjectList2280& objList) override
    {
        int NbObject = objList.getNbOfObjects();
        //header part
        double object_time = objList.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
        object_time = object_time /1000;
        objrs_time.fromSec(object_time);
        objtr_scantimestamp.push_back(objrs_time);
        // autoware_msgs::DetectedObjectArray temp_objList;
        ld_msgs::ld_object_list temp_objList;
        temp_objList.header.stamp = objrs_time;
        // std::cout << "Object List time is   " << objrs_time <<std::endl;
        temp_objList.header.frame_id = "innovusion";

        //frame number part defines in the main parts

        //		Objects = objList->getObjects();
        // autoware_msgs::DetectedObject temp_obj;
        ld_msgs::ld_object temp_obj;
        ObjectList2280::ObjectVector ibeo_obj;
        ibeo_obj = objList.getObjects();
        for (int j=0; j<NbObject; j++){
            // ------ID----------
            temp_obj.id = ibeo_obj[j].getObjectId();
            // ------LABEL----------
            switch(ibeo_obj[j].getClassification())
            {
            case ibeo::common::sdk::ObjectClass::Unclassified :{
                temp_obj.class_label_pred = "Unclassified";
                break;
            }
            case ibeo::common::sdk::ObjectClass::UnknownSmall :{
                temp_obj.class_label_pred = "UnknownSmall";
                break;
            }
            case ibeo::common::sdk::ObjectClass::UnknownBig:{
                temp_obj.class_label_pred = "UnknownBig";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Pedestrian:{
                temp_obj.class_label_pred = "Pedestrian";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Bike:{
                temp_obj.class_label_pred = "Bike";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Car:{
                temp_obj.class_label_pred = "Car";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Truck:{
                temp_obj.class_label_pred = "Truck";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Underdriveable:{
                temp_obj.class_label_pred = "Underdriveable";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Motorbike:{
                temp_obj.class_label_pred = "Motorcycle";
                break;
            }
            }

            // ------POSE---------- raw version ---
            temp_obj.pose.position.x = ibeo_obj[j].getObjBoxCenter().getX(); // -1.46
            temp_obj.pose.position.y = ibeo_obj[j].getObjBoxCenter().getY();
            temp_obj.pose.position.z = - 0.7;

            temp_obj.jsk_pose.position.x = ibeo_obj[j].getObjBoxCenter().getX(); // -1.46
            temp_obj.jsk_pose.position.y = ibeo_obj[j].getObjBoxCenter().getY();
            temp_obj.jsk_pose.position.z = - 0.7;

            temp_obj.pose_var.x = ibeo_obj[j].getObjBoxCenterSigma().getX();
            temp_obj.pose_var.y = ibeo_obj[j].getObjBoxCenterSigma().getY();


            double yaw = ibeo_obj[j].getObjBoxOrientation();
            double roll = 0;
            double pitch = 0;
            double cy = cos(yaw/2);
            double sy = sin(yaw/2);
            double cp = cos(pitch/2);
            double sp = sin(pitch/2);
            double cr = cos(roll/2);
            double sr = sin(roll/2);

            temp_obj.pose.orientation.w = cy * cp * cr + sy * sp * sr;
            temp_obj.pose.orientation.x = cy * cp * sr - sy * sp * cr;
            temp_obj.pose.orientation.y = sy * cp * sr + cy * sp * cr;
            temp_obj.pose.orientation.z = sy * cp * cr - cy * sp * sr;

            temp_obj.jsk_pose.orientation.w = cy * cp * cr + sy * sp * sr;
            temp_obj.jsk_pose.orientation.x = cy * cp * sr - sy * sp * cr;
            temp_obj.jsk_pose.orientation.y = sy * cp * sr + cy * sp * cr;
            temp_obj.jsk_pose.orientation.z = sy * cp * cr - cy * sp * sr;

            // ------DIMENSION---------- raw version ---
            temp_obj.dimensions.x = ibeo_obj[j].getObjBoxSize().getX();
            temp_obj.dimensions.y = ibeo_obj[j].getObjBoxSize().getY();
            temp_obj.dimensions.z = 2;

            // ------VELOCITY---------- raw version ---
            temp_obj.velocity.linear.x = ibeo_obj[j].getAbsVelocity().getX();
            temp_obj.velocity.linear.y = ibeo_obj[j].getAbsVelocity().getY();
            temp_obj.velocity.linear.z = 0;


            temp_obj.yaw_var = ibeo_obj[j].getObjBoxOrientationSigma();
            temp_obj.velocity_var.linear.x = ibeo_obj[j].getAbsVelocitySigma().getX();
            temp_obj.velocity_var.linear.y = ibeo_obj[j].getAbsVelocitySigma().getY();

            //		std::cout << "id of this object is " << temp_obj.id << "label of this object is " << temp_obj.label <<std::endl;
            //		temp_obj.id = objList->getObjects()
            //	    Objects.push_back(temp_obj);
            temp_objList.objects.push_back(temp_obj);
        }
        ObjectsList.push_back(temp_objList);
        flag_2280 = 1;
    }
     //========================================
    void onData(const ObjectList2281& objList) override
    {

        //  x_s.push_back(objList.getHeaderNtpTime());
        int NbObject = objList.getNbOfObjects();
        //header part
        double object_time = objList.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
        object_time = object_time /1000;
        objrs_time.fromSec(object_time);
        objtr_scantimestamp.push_back(objrs_time);
        // autoware_msgs::DetectedObjectArray temp_objList;
        ld_msgs::ld_object_list temp_objList;
        temp_objList.header.stamp = objrs_time;
        // std::cout << "Object List time is   " << objrs_time <<std::endl;
        temp_objList.header.frame_id = "innovusion";

        // frame number part defines in the main parts


        //		Objects = objList->getObjects();
        // autoware_msgs::DetectedObject temp_obj;
        ld_msgs::ld_object temp_obj;
        ObjectList2281::ObjectVector ibeo_obj;
        ibeo_obj = objList.getObjects();
        for (int j=0; j<NbObject; j++){
            // ------ID----------
            temp_obj.id = ibeo_obj[j].getObjectId();
            // ------LABEL----------
            switch(ibeo_obj[j].getClassification())
            {
            case ibeo::common::sdk::ObjectClass::Unclassified :{
                temp_obj.class_label_pred = "Unclassified";
                break;
            }
            case ibeo::common::sdk::ObjectClass::UnknownSmall :{
                temp_obj.class_label_pred = "UnknownSmall";
                break;
            }
            case ibeo::common::sdk::ObjectClass::UnknownBig:{
                temp_obj.class_label_pred = "UnknownBig";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Pedestrian:{
                temp_obj.class_label_pred = "Pedestrian";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Bike:{
                temp_obj.class_label_pred = "Bike";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Car:{
                temp_obj.class_label_pred = "Car";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Truck:{
                temp_obj.class_label_pred = "Truck";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Underdriveable:{
                temp_obj.class_label_pred = "Underdriveable";
                break;
            }
            case ibeo::common::sdk::ObjectClass::Motorbike:{
                temp_obj.class_label_pred = "Motorcycle";
                break;
            }
            }

            // ------POSE---------- raw version ---
            temp_obj.pose.position.x = ibeo_obj[j].getObjectBoxCenter().getX(); // -1.46
            temp_obj.pose.position.y = ibeo_obj[j].getObjectBoxCenter().getY();
            temp_obj.pose.position.z = - 0.7;

            temp_obj.jsk_pose.position.x = ibeo_obj[j].getObjectBoxCenter().getX(); // -1.46
            temp_obj.jsk_pose.position.y = ibeo_obj[j].getObjectBoxCenter().getY();
            temp_obj.jsk_pose.position.z = - 0.7;

            double yaw = ibeo_obj[j].getYawRate();
            double roll = 0;
            double pitch = 0;
            double cy = cos(yaw/2);
            double sy = sin(yaw/2);
            double cp = cos(pitch/2);
            double sp = sin(pitch/2);
            double cr = cos(roll/2);
            double sr = sin(roll/2);

            temp_obj.pose.orientation.w = cy * cp * cr + sy * sp * sr;
            temp_obj.pose.orientation.x = cy * cp * sr - sy * sp * cr;
            temp_obj.pose.orientation.y = sy * cp * sr + cy * sp * cr;
            temp_obj.pose.orientation.z = sy * cp * cr - cy * sp * sr;

            temp_obj.jsk_pose.orientation.w = cy * cp * cr + sy * sp * sr;
            temp_obj.jsk_pose.orientation.x = cy * cp * sr - sy * sp * cr;
            temp_obj.jsk_pose.orientation.y = sy * cp * sr + cy * sp * cr;
            temp_obj.jsk_pose.orientation.z = sy * cp * cr - cy * sp * sr;

            // ------DIMENSION---------- raw version ---
            temp_obj.dimensions.x = ibeo_obj[j].getObjectBoxSize().getX();
            temp_obj.dimensions.y = ibeo_obj[j].getObjectBoxSize().getY();
            temp_obj.dimensions.z = ibeo_obj[j].getObjectHeight();

            // ------VELOCITY---------- raw version ---
            temp_obj.velocity.linear.x = ibeo_obj[j].getAbsoluteVelocity().getX();
            temp_obj.velocity.linear.y = ibeo_obj[j].getAbsoluteVelocity().getY();
            temp_obj.velocity.linear.z = 0;

            //		std::cout << "id of this object is " << temp_obj.id << "label of this object is " << temp_obj.label <<std::endl;
            //		temp_obj.id = objList->getObjects()
            //	    Objects.push_back(temp_obj);
            temp_objList.objects.push_back(temp_obj);
        }
        ObjectsList.push_back(temp_objList);
        flag_2281 = 1;
    }
 //========================================
    void onData(const Image2403& image) override
    {
        // spdlog::info("Image2403 received: time: {0}", tc.toString(image.getTimestamp().toPtime()));

// #ifdef WITHJPEGSUPPORT
       if ((image.getFormat() == image::ImageFormatIn2403::Jpeg) || (image.getFormat() == image::ImageFormatIn2403::Mjpeg))
        {
            int id;
            id = image.getDeviceId();
            // std::cout << "device id is " << id << std::endl;

            const char* rawBuffer;
            unsigned int compressedSize;
            rawBuffer = &(*image.getImageBuffer()->getDataBuffer().data());
            compressedSize = image.getImageBuffer()->getDataBuffer().size();

            if( id == camera_id.at(0)){
            ld_msgs::ld_image img;
            unsigned int width  = image.getWidth();
            unsigned int height = image.getHeight();
            double temp_time = image.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
            temp_time = temp_time / 1000;
            rs_time.fromSec(temp_time);
            img.header.stamp = rs_time;
	        img.image_format = 0;
	        img.device_id = camera_id.at(1);
	        img.image_height = height;
	        img.image_width = width;
      	    img.compressed_size = compressedSize;

	        img.compressed_data.resize(compressedSize + 100);
	        memcpy(&img.compressed_data[0], rawBuffer, compressedSize);
            img_Fs.push_back(img);
           }

           else if (id == camera_id.at(1)){
            ld_msgs::ld_image img;
            unsigned int width  = image.getWidth();
            unsigned int height = image.getHeight();
            double temp_time = image.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
            temp_time = temp_time / 1000;
            rs_time.fromSec(temp_time);
            img.header.stamp = rs_time;
            img.image_format = 0;
	        img.device_id = camera_id.at(1);
	        img.image_height = height;
	        img.image_width = width;
	        img.compressed_size = compressedSize;
	        img.compressed_data.resize(compressedSize + 100);

	        memcpy(&img.compressed_data[0], rawBuffer,compressedSize);
            img_Bs.push_back(img);
     }
    }
    }

}; // AllListener

// std::string IbeoNtpTime2Str(const ibeo::common::sdk::NTPTime& time);

int main(int argc, char** argv) {
   int currArg =1;
   std::string idc_filepath = argv[currArg++];
   std::string iv_filepath = argv[currArg++];
   std::string opt_path = argv[currArg++];
   std::string sync_type = argv[currArg++];  // idc or can
   std::vector<sensor_msgs::PointCloud2>  ros_cloud;

//    spdlog::info("argc is : {0}", argc);
//    if(argc == 6){
//        std::string img_filepath = argv[6];

//    }
//    spdlog::info(img_filepath);

  // ============ innovusion or ouster ===============

  std::string yaml_1= "./config/cali.yaml";
  YAML::Node cali = YAML::LoadFile(yaml_1);
  YAML::Node node1, node2, node3, node4;
  node1 = cali["Topic Name"];
  node2 = cali["Timestamp position"];
  node3 = cali["Vehicle Type"];
  node4 = cali["Camera ID"];
  para_topic.push_back(node1[0].as<std::string>());
  para_topic.push_back(node2[0].as<std::string>());
  para_topic.push_back(node3[0].as<std::string>());
  // LiDAR Sensor coordinate transform to vehicle coordinate
  yamlParser yaml;
  float yaml_x = yaml.get_trans_vec().x;
  float yaml_y = yaml.get_trans_vec().y;
  float yaml_z = yaml.get_trans_vec().z;
//   float yaml_z = 0;
  float yaml_yaw = yaml.get_trans_vec().yaw;
  float yaml_pitch = yaml.get_trans_vec().pitch;
  float yaml_roll = yaml.get_trans_vec().roll;
//   spdlog::info("pitch up value is : {0}", yaml_x);

  int id_1 = node4[0].as<int>();
  int id_2 = node4[1].as<int>();
  camera_id.push_back(id_1);
  camera_id.push_back(id_2);
  spdlog::info("id 1 is : {0}", id_1);
  // ===================== auflanden CAN yaml ==================

  YAML::Node can;
  if(para_topic.at(2) == "passat"){
    can = YAML::LoadFile("./config/CAN_vw.yaml");
  }
  else if(para_topic.at(2) == "haval"){
    can = YAML::LoadFile("./config/CAN_haval.yaml");
  }
//   YAML::Node velo = can["velocity"];
//   spdlog::info("velo type is : {0}  !!!!", can["velocity"]["Identifier"].as<int>());
//   spdlog::info("velo factor is : {0}", can["velocity"]["Factor"].as<float>());


  // velocity
  uint32_t id = can["velocity"]["Identifier"].as<int>();
  velocity.set_id(id);
  double factor = can["velocity"]["Factor"].as<double>();
  velocity.set_factor(factor);
  uint8_t firstb = can["velocity"]["LSB"].as<int>();
  velocity.set_lsb(firstb);
  bool is_bigendian = can["velocity"]["UseBigEndian"].as<bool>();
  velocity.set_is_big_endian(is_bigendian);
  uint8_t lastb = can["velocity"]["MSB"].as<int>();
  velocity.set_msb(lastb);
  double offset = can["velocity"]["Offset"].as<double>();
  velocity.set_offset(offset);
  uint8_t signb = can["velocity"]["SignBit"].as<int>();
  velocity.set_sign_bit(signb);
  bool signa = can["velocity"]["SignBitAvailable"].as<bool>();
  velocity.set_signbit_available(signa);
  double trans = can["velocity"]["UnitTrans"].as<double>();
  velocity.set_unit_trans(trans);
  bool two_com = can["velocity"]["TwoComplement"].as<bool>();
  velocity.set_two_complement(two_com);
  id_list.push_back(id);

  // yawrate
  uint32_t yaw_id = can["yawrate"]["Identifier"].as<int>();
//   spdlog::info(yaw_id);
  yaw.set_id(yaw_id);
  double yaw_factor = can["yawrate"]["Factor"].as<double>();
  yaw.set_factor(yaw_factor);
  uint8_t yaw_firstb = can["yawrate"]["LSB"].as<int>();
  yaw.set_lsb(yaw_firstb);
  bool yaw_is_bigendian = can["yawrate"]["UseBigEndian"].as<bool>();
  yaw.set_is_big_endian(yaw_is_bigendian);
  uint8_t yaw_lastb = can["yawrate"]["MSB"].as<int>();
  yaw.set_msb(yaw_lastb);
//   spdlog::info(yaw_lastb);
  double yaw_offset = can["yawrate"]["Offset"].as<double>();
  yaw.set_offset(yaw_offset);
  uint8_t yaw_signb = can["yawrate"]["SignBit"].as<int>();
  yaw.set_sign_bit(yaw_signb);
  bool yaw_signa = can["yawrate"]["SignBitAvailable"].as<bool>();
  yaw.set_signbit_available(yaw_signa);
  std::cout << "yaw_signa   " << yaw_signa  << std::endl;
  double yaw_trans = can["yawrate"]["UnitTrans"].as<double>();
  yaw.set_unit_trans(yaw_trans);
  bool yaw_two_com = can["yawrate"]["TwoComplement"].as<bool>();
  yaw.set_two_complement(yaw_two_com);
  id_list.push_back(yaw_id);
//   std::cout << "id selectd is " << id_list.at(0)  << "   " << id_list.at(1) << std::endl;


  uint32_t x_id = can["acc_x"]["Identifier"].as<int>();
//   spdlog::info(yaw_id);
  accx.set_id(x_id);
  double x_factor = can["acc_x"]["Factor"].as<double>();
  accx.set_factor(x_factor);
  uint8_t x_firstb = can["acc_x"]["LSB"].as<int>();
  accx.set_lsb(x_firstb);
  bool x_is_bigendian = can["acc_x"]["UseBigEndian"].as<bool>();
  accx.set_is_big_endian(x_is_bigendian);
  uint8_t x_lastb = can["acc_x"]["MSB"].as<int>();
  accx.set_msb(x_lastb);
//   spdlog::info(yaw_lastb);
  double x_offset = can["acc_x"]["Offset"].as<double>();
  accx.set_offset(x_offset);
  uint8_t x_signb = can["acc_x"]["SignBit"].as<int>();
  accx.set_sign_bit(x_signb);
  bool x_signa = can["acc_x"]["SignBitAvailable"].as<bool>();
  accx.set_signbit_available(x_signa);
  double x_trans = can["acc_x"]["UnitTrans"].as<double>();
  accx.set_unit_trans(x_trans);
  bool x_two_com = can["acc_x"]["TwoComplement"].as<bool>();
  accx.set_two_complement(x_two_com);

  uint32_t y_id = can["acc_y"]["Identifier"].as<int>();
//   spdlog::info(yaw_id);
  accy.set_id(y_id);
  double y_factor = can["acc_y"]["Factor"].as<double>();
  accy.set_factor(y_factor);
  uint8_t y_firstb = can["acc_y"]["LSB"].as<int>();
  accy.set_lsb(y_firstb);
  bool y_is_bigendian = can["acc_y"]["UseBigEndian"].as<bool>();
  accy.set_is_big_endian(y_is_bigendian);
  uint8_t y_lastb = can["acc_y"]["MSB"].as<int>();
  accy.set_msb(y_lastb);
//   spdlog::info(yaw_lastb);
  double y_offset = can["acc_y"]["Offset"].as<double>();
  accy.set_offset(y_offset);
  uint8_t y_signb = can["acc_y"]["SignBit"].as<int>();
  accy.set_sign_bit(y_signb);
  bool y_signa = can["acc_y"]["SignBitAvailable"].as<bool>();
  accy.set_signbit_available(y_signa);
  double y_trans = can["acc_y"]["UnitTrans"].as<double>();
  accy.set_unit_trans(y_trans);
  bool y_two_com = can["acc_y"]["TwoComplement"].as<bool>();
  accy.set_two_complement(y_two_com);

   spdlog::info("CAN yaml loaded!");


  // ============= idc pc sync ==================
  if (sync_type == "idc"){

  std::vector<std::string> idc_files, iv_bagfiles;  // innovusion files (paths) from the
                                         // folder for being processed
  std::vector<std::string> idc_bagfilenames, iv_bagfilenames;  // innovusion files (names) from
                                             // the folder for post-processed
                                             // file names
  std::vector<std::string> IDCfilenames, BAGfilenames;
  FileManager file_handler;

  file_handler.GetAllFormatFiles(idc_filepath, idc_files, ".idc", idc_bagfilenames, IDCfilenames);
  file_handler.GetAllFormatFiles(iv_filepath, iv_bagfiles, ".bag", iv_bagfilenames, BAGfilenames);
  std::sort(iv_bagfiles.begin(),iv_bagfiles.end());
  std::sort(iv_bagfilenames.begin(), iv_bagfilenames.end());
  std::sort(BAGfilenames.begin(), BAGfilenames.end());

  std::vector<float> paras;
  bool is_paras_given;
  paras = {yaml_x, yaml_y, yaml_z, yaml_yaw, yaml_pitch, yaml_roll};
  is_paras_given = 1;
  spdlog::info("bag size is : {0}", iv_bagfiles.size());


   for(int idx=0;idx<idc_files.size();idx++){
   IdcFile idc_file_;

   idc_file_.open(idc_files[idx]);
    if (!idc_file_.isOpen()) {
        spdlog::error("Can't open idc file. The file path is {0}.", idc_filepath);
        return false;
    }


    // can_file.open("CANMsg.csv");
    // can_file << "yawrate" << "," << "acceleration_x" << "," << "acceration_y" << std::endl;


    AllListener allListener;
    idc_file_.registerContainerListener(&allListener);

    // uint32_t nbMessages = 0; // # of messages we parsed
   std::unique_ptr<IdcFileController> idc_ctrl_{nullptr};
   idc_ctrl_= idc_file_.createController();

   FrameIndex6130::FrameVector frame_indices_{};
   frame_indices_ = idc_file_.getFrameIndex().getFrameIndices();
//    spdlog::info("frame index size is: {0}", frame_indices_.size());
   IbeoDataHeader dh_temp;
   const uint32_t max_frame_num = frame_indices_.size();
//    spdlog::info("max frame num is: {0}", max_frame_num);

    idc_ctrl_->setPos(frame_indices_[0].getFilePosition());
    idc_ctrl_->getNextHeader(dh_temp);
    double file_start_t = dh_temp.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
    ros::Time file_start_ros;
    file_start_ros.fromSec(file_start_t /1000) ;
    std::cout << "start ros time is " << file_start_ros << std::endl;

    idc_ctrl_->setPos(frame_indices_[max_frame_num - 1].getFilePosition());
    idc_ctrl_->getNextHeader(dh_temp);
    double file_end_t = dh_temp.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
    ros::Time file_end_ros;
    file_end_ros.fromSec(file_end_t /1000) ;
    std::cout << "end ros time is " << file_end_ros << std::endl;

    std::vector<ros::Time> idc_time;
    for(int idx = 0; idx < max_frame_num; idx++){
        idc_ctrl_->setPos(frame_indices_[idx].getFilePosition());
        idc_ctrl_->getNextHeader(dh_temp);
        double temp_t = dh_temp.getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() - 2208988800000;
        ros::Time temp_ros;
        temp_ros.fromSec(temp_t /1000) ;
        idc_time.push_back(temp_ros);
    }
    spdlog::info("idc time size : {0}", idc_time.size());

    std::vector<int> id_sync;
    for (int ii = 0 ; ii < iv_bagfiles.size(); ii++){
    std::vector<ros::Time> iv_tStmp;
    rosbag::Bag iv_bag;
    iv_bag.open(iv_bagfiles.at(ii), rosbag::bagmode::Read);
    // std::vector<std::string> iv_topics;
    // iv_topics.push_back("/iv_points");
    // rosbag::View iv_view(iv_bag, rosbag::TopicQuery(iv_topics));
    // std::vector<sensor_msgs::PointCloud2> iv_ptClouds;
    // foreach(rosbag::MessageInstance const m, iv_view)
    // {
    //      sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
    //      if((int)floor(s->header.stamp.toSec()) - 5 <=  floor(idc_time.front().toSec()) || s->header.stamp.toSec() >= floor(idc_time.back().toSec()) ){  // || s->header.stamp.toSec() >= floor(idc_time.back().toSec())
    //          spdlog::info("innovusion bag not included in current idc!!");
    //          break;
    //      }
    //      iv_tStmp.push_back(s->header.stamp);
    //      iv_ptClouds.push_back(*s);
    // }
    rosbag::View iv_view(iv_bag, rosbag::TopicQuery(para_topic.at(0)));
    std::vector<sensor_msgs::PointCloud2> iv_ptClouds;
    foreach(rosbag::MessageInstance const m, iv_view){
        sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
        if(para_topic.at(1) == "header"){
            if((int)floor(s->header.stamp.toSec()) - 5 <=  floor(idc_time.front().toSec()) || s->header.stamp.toSec() >= floor(idc_time.back().toSec()) ){  // || s->header.stamp.toSec() >= floor(idc_time.back().toSec())
             spdlog::info("innovusion bag not included in current idc!!");
             break;
            }
            iv_tStmp.push_back(s->header.stamp);
            iv_ptClouds.push_back(*s);
        }
        else if (para_topic.at(1) == "bag"){
            if((int)floor(m.getTime().toSec()) - 5 <=  floor(idc_time.front().toSec()) || m.getTime().toSec() >= floor(idc_time.back().toSec()) ){  // || s->header.stamp.toSec() >= floor(idc_time.back().toSec())
             spdlog::info("innovusion bag not included in current idc!!");
             break;
            }
            iv_tStmp.push_back(m.getTime());
            iv_ptClouds.push_back(*s);
        }
    }
    if (iv_ptClouds.size() == 0) continue;
    spdlog::info("bag pc size is : {0} {1}", ii , iv_ptClouds.size());
    iv_bag.close();
    ros::Time T1 = iv_tStmp.front();
    ros::Time T2 = iv_tStmp.back();

    int bd_limit = 5 ;
    int iv_start_time = (int)floor(iv_tStmp.front().toSec()) - bd_limit;
    int iv_end_time = (int)floor(iv_tStmp.back().toSec()) + bd_limit;
    if( iv_start_time <= (int)floor(idc_time.front().toSec()) || iv_end_time >= (int)floor(idc_time.back().toSec())){
        spdlog::info("seems invalid pointcloud : {0}", BAGfilenames.at(ii));
        continue;
    }
    else{
        // find the closet idc index to iv_start time
        std::vector<ros::Time>::iterator iter;
        std::vector<int> sub_frame;
        for (iter = idc_time.begin() ; iter != idc_time.end() ; ++iter){

            if ((*iter - T1).toSec() >= 0 && (*iter - T2).toSec() <= 0){
                sub_frame.push_back(std::distance(idc_time.begin(),iter));
            }
        }
        uint64_t start_pos = sub_frame.front()-5;
        uint64_t end_pos = frame_indices_[sub_frame.back()+5].getFilePosition();
        // std::cout << frame_indices_[start_pos].getFilePosition() << " to " << frame_indices_[sub_frame.back()+5].getFilePosition() << std::endl;
        idc_ctrl_->setPos(frame_indices_[start_pos].getFilePosition());
        idc_ctrl_->getNextHeader(dh_temp);
         int nbMessages = 0;
         while(idc_file_.isGood()){
            //  std::cout << "current position is " << idc_ctrl_->getPos() << std::endl;
         IdcFile::ContainerImporterPairs p = idc_file_.getNextDataContainer();
         idc_file_.notifyContainerListeners(p);
         ++nbMessages;
         if (idc_ctrl_->getPos() >= end_pos) {
             break;
             }
         }
        spdlog::info("pc is: {0}", pcl_pointclouds.size());
        spdlog::info("ObjectList is : {0}", ObjectsList.size());
        spdlog::info("can msg 1 is : {0}", v_can_velocity.size());

        // std::cout << ii << " bag, pc start time stamp is " << tr_scantimestamp.at(0)<< std::endl;

        // rosbag::Bag bag_write;
        // std::string filename = "idc2rosbag_" + std::to_string(ii) + ".bag";
        // bag_write.open(filename, rosbag::bagmode::Write);
        // std::vector<autoware_msgs::DetectedObjectArray> idc_Objlist_NO;
        // std::vector<autoware_msgs::DetectedObjectArray> idc_Objlist_YES;

        for (int in=0; in<pcl_pointclouds.size();in++ ){

            sensor_msgs::PointCloud2 ros_point;
            pcl::toROSMsg( pcl_pointclouds[in], ros_point);
            ros_cloud.push_back(ros_point);
            ros_cloud[in].header.frame_id = "innovusion";
            ros_cloud[in].header.stamp = tr_scantimestamp[in];

            // bag_write.write("/lux_pointcloud",tr_scantimestamp[in], ros_cloud[in]);
        }
        // for (int i = 0; i < v_can_yawrate.size(); i++){
        //     bag_write.write("/lux_yawrate", yaw_ts.at(i), v_can_yawrate.at(i));
        // }
        // for (int i = 0; i < v_can_velocity.size(); i++){
        //     bag_write.write("/lux_velocity", velo_ts.at(i), v_can_velocity.at(i));
        // }
        // std::vector<autoware_msgs::DetectedObjectArray> idc_Objlist_NO;
        // std::vector<autoware_msgs::DetectedObjectArray> idc_Objlist_YES;
        std::vector<ld_msgs::ld_object_list> idc_Objlist_NO;
        std::vector<ld_msgs::ld_object_list> idc_Objlist_YES;
        if (flag_2281 == 1 && flag_2280 == 0){
            for (int j=0; j<ObjectsList.size();j=j+2){
            // bag_write.write("/lux_ObjectList_no",objtr_scantimestamp[j], ObjectsList[j]);
             idc_Objlist_YES.push_back(ObjectsList[j]);
            }
            for (int j=1; j<ObjectsList.size();j=j+2){
            // bag_write.write("/lux_ObjectList_yes",objtr_scantimestamp[j], ObjectsList[j]);
               idc_Objlist_NO.push_back(ObjectsList[j]);
             }
        }
        if (flag_2281 == 0 && flag_2280 == 1){
            for (int j=0; j<ObjectsList.size();j++){
            // bag_write.write("/lux_ObjectList_no",objtr_scantimestamp[j], ObjectsList[j]);
             idc_Objlist_YES.push_back(ObjectsList[j]);
             idc_Objlist_NO.push_back(ObjectsList[j]);
            }
        }

        // bag_write.close();
        spdlog::info("objList size is : {0}" , idc_Objlist_NO.size());
        // ============ sync step begins ===============
        int idc2iv_Idx = 0;
        int idx_idc2iv_can = 0;
        // ============= can interpolation ===============
        interpolation can_inter(iv_tStmp, v_can_velocity, v_can_yawrate);
        spdlog::info("interpolated can msg size is : {0}", can_inter.get_vec_accy_fq().size());
        spdlog::info("interpolated can msg size is : {0}", can_inter.get_vec_yawrate_fq().size());
        spdlog::info("interpolated can msg size is : {0}", can_inter.get_vec_velocity_fq().size());
        std::vector<double> velo;
        std::vector<double> yawrate;
        std::vector<double> accx;
        std::vector<double> accy;
        velo = can_inter.get_vec_velocity_fq();
        yawrate = can_inter.get_vec_yawrate_fq();
        accx = can_inter.get_vec_accx_fq();
        accy = can_inter.get_vec_accy_fq();
        // velo.push_back(0);
        spdlog::info("interpolated can msg size is : {0}", velo.size());
        ld_msgs::ld_imugps_can can_sync2pc;
        std::vector<ld_msgs::ld_imugps_can> v_can_sync2pc;
        for (int idx_inno = 0; idx_inno < iv_tStmp.size(); idx_inno++)
        {
           if (idx_inno > velo.size()-1){
               velo.push_back(0);
           }
           if (idx_inno > yawrate.size()-1){
               yawrate.push_back(0);
           }
           if (idx_inno > accx.size()-1){
               accx.push_back(0);
           }
           if (idx_inno > accy.size()-1){
               accy.push_back(0);
           }
           can_sync2pc.acceleration_x = accx.at(idx_inno);
           can_sync2pc.acceleration_y = accy.at(idx_inno);
           can_sync2pc.yawrate = yawrate.at(idx_inno);
           can_sync2pc.v_x_CAN = velo.at(idx_inno);
           can_sync2pc.header.stamp = iv_tStmp.at(idx_inno);
        // std::cout << idx_inno << " frame, yawrate is " << can_sync2pc.yawrate << ", acce_x is " << can_sync2pc.acceleration_x  << ", acce y is " << can_sync2pc.acceleration_y << ", velo is " << can_sync2pc.v_x_CAN << std::endl;
        v_can_sync2pc.push_back(can_sync2pc);
        //		std::cout << finalTime.at(idx_inno) << std::endl;
        }
        std::cout << "61 is " << v_can_sync2pc.at(61).acceleration_x <<  " 62 is " << v_can_sync2pc.at(62).acceleration_x  << std::endl;
        // ================== write bag ========================
        std::vector<ros::Time> idc2ivTime;
        std::vector<sensor_msgs::PointCloud2> idc2iv_ptClouds;
        // std::vector<autoware_msgs::DetectedObjectArray> idc2iv_Objlist_NO;
        // std::vector<autoware_msgs::DetectedObjectArray> idc2iv_Objlist_YES;
        std::vector<ld_msgs::ld_object_list> idc2iv_Objlist_NO;
        std::vector<ld_msgs::ld_object_list> idc2iv_Objlist_YES;

        for (unsigned int idx_iv = 0; idx_iv < iv_tStmp.size(); idx_iv++)
        {
            ros::Duration minTD = iv_tStmp.at(idx_iv) - tr_scantimestamp.at(0);
        // Run through the idc time stamps and find the smallest time difference
             for (int idx_idc = 0; idx_idc < tr_scantimestamp.size(); ++idx_idc)
             {
                ros::Duration crtTD = iv_tStmp.at(idx_iv) - tr_scantimestamp.at(idx_idc);
                if (crtTD < minTD && crtTD > ros::Duration(0.0))
               {
                    minTD = crtTD;
                    idc2iv_Idx = idx_idc;
                // break;
               }
              }
    //     //        idc2ivTime.push_back(idc_tStmp.at(idc2iv_Idx));
        idc2iv_ptClouds.push_back(ros_cloud.at(idc2iv_Idx));
        idc2iv_Objlist_NO.push_back(idc_Objlist_NO.at(idc2iv_Idx));
        idc2iv_Objlist_YES.push_back(idc_Objlist_YES.at(idc2iv_Idx));
        }
        spdlog::info("after sync, pc size is : {0}", idc2iv_ptClouds.size());
        if (idc2iv_ptClouds.size() == 0 || idc2iv_Objlist_NO.size() == 0 || idc2iv_Objlist_YES.size() == 0 || v_can_sync2pc.size() == 0)
        {
        spdlog::error("***Something is WRONG, probably innovusion time stamps are not fully covered by ibeo(.idc).***");
        break;
        }


        rosbag::Bag write_bag;

        const int dot_pos = iv_bagfilenames.at(ii).find(".");
        std::string iv_name_tobe_added = iv_bagfilenames.at(ii).substr(0, dot_pos);

        const std::string sync_file = opt_path +"/"+ iv_name_tobe_added +"_sync.bag";
        write_bag.open(sync_file, rosbag::bagmode::Write);
       // ============ without coordinate transform ============
        for (int idx_all = 0; idx_all < iv_ptClouds.size(); idx_all++)
        {
          if(para_topic.at(0) == "/iv_points"){
          pcl::PointCloud<ld::ivStruct> pclcloud_iv_raw;
          pcl::PointCloud<ld::ivStruct> pclcloud_iv_t;

          sensor_msgs::PointCloud2 iv_pc;

          pcl::fromROSMsg (iv_ptClouds.at(idx_all), pclcloud_iv_raw);
          pcl::fromROSMsg (iv_ptClouds.at(idx_all), pclcloud_iv_t);

          Eigen::Affine3f tf_mat = Eigen::Affine3f::Identity();


          float x = yaml.get_coor_vec().X;
          float y = yaml.get_coor_vec().Y;
          float z = yaml.get_coor_vec().Z;

          double qw = yaml.get_coor_vec().qw;
          double qx = yaml.get_coor_vec().qx;
          double qy = yaml.get_coor_vec().qy;
          double qz = yaml.get_coor_vec().qz;

        Eigen::Vector3f trans_inno2velo(x, y, z);
        Eigen::Quaternionf quat_inno2velo(qw, qx, qy, qz);

    // 雷达标定位置四元数   from yaml file
        float trans_x = paras.at(0);
        float trans_y = paras.at(1);
        float trans_z = paras.at(2);
        float yaw = paras.at(3)*3.14/180;
        float pitch = paras.at(4)*3.14/180;
        float roll = paras.at(5)*3.14/180;

        float cy = cos(yaw*0.5);
        float sy = sin(yaw*0.5);
        float cp = cos(pitch*0.5);
        float sp = sin(pitch*0.5);
        float cr = cos(roll*0.5);
        float sr = sin(roll*0.5);

        float quat_w = cy * cp * cr + sy * sp * sr;
        float quat_x = cy * cp * sr - sy * sp * cr;
        float quat_y = sy * cp * sr + cy * sp * cr;
        float quat_z = sy * cp * cr - cy * sp * sr;

        Eigen::Vector3f trans_velo2car(trans_x, trans_y, trans_z);
        Eigen::Quaternionf rota_velo2car(quat_w, quat_x, quat_y, quat_z);


        tf_mat.translate(trans_inno2velo);
        tf_mat.rotate(quat_inno2velo);

        Eigen::Affine3f tf_mat_ = Eigen::Affine3f::Identity();
        tf_mat_.translate(trans_velo2car);
        tf_mat_.rotate(rota_velo2car);

         //整体转换矩阵生成
         tf_mat=tf_mat_*tf_mat;

        pcl::transformPointCloud(pclcloud_iv_raw, pclcloud_iv_t, tf_mat);


        pcl::toROSMsg(pclcloud_iv_t, iv_pc);
        iv_pc.header.frame_id = "innovusion";

        write_bag.write(para_topic.at(0), iv_tStmp.at(idx_all), iv_pc);
        write_bag.write("/lux_pointcloud", iv_tStmp.at(idx_all), idc2iv_ptClouds.at(idx_all));
          }
        if (para_topic.at(0) == "/os1_cloud_node/points"){

            pcl::PointCloud<ld::osStruct> pclcloud_iv_raw;
            pcl::PointCloud<ld::osStruct> pclcloud_iv_t;
            sensor_msgs::PointCloud2 iv_pc;

            pcl::fromROSMsg (iv_ptClouds.at(idx_all), pclcloud_iv_raw);
            pcl::fromROSMsg (iv_ptClouds.at(idx_all), pclcloud_iv_t);

            Eigen::Affine3f tf_mat = Eigen::Affine3f::Identity();


            double x = yaml.get_coor_vec().X;
            double y = yaml.get_coor_vec().Y;
            double z = yaml.get_coor_vec().Z;

            double qw = yaml.get_coor_vec().qw;
            double qx = yaml.get_coor_vec().qx;
            double qy = yaml.get_coor_vec().qy;
            double qz = yaml.get_coor_vec().qz;

        Eigen::Vector3f trans_inno2velo(x, y, z);
        Eigen::Quaternionf quat_inno2velo(qw, qx, qy, qz);

    // 雷达标定位置四元数   from yaml file
        float trans_x = paras.at(0);
        float trans_y = paras.at(1);
        float trans_z = paras.at(2);
        float yaw = paras.at(3)*3.14/180;
        float pitch = paras.at(4)*3.14/180;
        float roll = paras.at(5)*3.14/180;

        float cy = cos(yaw*0.5);
        float sy = sin(yaw*0.5);
        float cp = cos(pitch*0.5);
        float sp = sin(pitch*0.5);
        float cr = cos(roll*0.5);
        float sr = sin(roll*0.5);

        float quat_w = cy * cp * cr + sy * sp * sr;
        float quat_x = cy * cp * sr - sy * sp * cr;
        float quat_y = sy * cp * sr + cy * sp * cr;
        float quat_z = sy * cp * cr - cy * sp * sr;

        Eigen::Vector3f trans_velo2car(trans_x, trans_y, trans_z);
        Eigen::Quaternionf rota_velo2car(quat_w, quat_x, quat_y, quat_z);


        tf_mat.translate(trans_inno2velo);
        tf_mat.rotate(quat_inno2velo);

        Eigen::Affine3f tf_mat_ = Eigen::Affine3f::Identity();
        tf_mat_.translate(trans_velo2car);
        tf_mat_.rotate(rota_velo2car);

         //整体转换矩阵生成
         tf_mat=tf_mat_*tf_mat;

        pcl::transformPointCloud(pclcloud_iv_raw, pclcloud_iv_t, tf_mat);

        pcl::toROSMsg(pclcloud_iv_t, iv_pc);
        // iv_pc.header.frame_id = "innovusion";
        idc2iv_ptClouds.at(idx_all).header.frame_id = "os1_lidar";
        write_bag.write(para_topic.at(0), iv_tStmp.at(idx_all), iv_pc);
        write_bag.write("/lux_pointcloud", iv_tStmp.at(idx_all), idc2iv_ptClouds.at(idx_all));
        }
        // idc2iv_ptClouds.at(idx_all).header.stamp = iv_tStmp.at(idx_all);

        write_bag.write("/lux_ObjectList_no", iv_tStmp.at(idx_all), idc2iv_Objlist_NO.at(idx_all));
        write_bag.write("/lux_ObjectList_yes", iv_tStmp.at(idx_all), idc2iv_Objlist_YES.at(idx_all));
        write_bag.write("/ld_imugps_can", iv_tStmp.at(idx_all), v_can_sync2pc.at(idx_all));
       }

       // write image
       uint64_t diff = iv_tStmp.at(0).toNSec() - img_Fs.at(0).header.stamp.toNSec();
       double diff_f = diff / 1000000000.00;
       ros::Duration diff_dr;
       diff_dr = ros::Duration(diff_f);
    //    double diff = img_Fs.at(0).header.stamp.toSec() - iv_tStmp.at(0).toSec();
       std::cout << "diff is " << img_Fs.at(1).header.stamp - img_Fs.at(0).header.stamp << std::endl;
       std::cout << "image timestamp is " << std::fixed << diff_dr << ", camera and inno time differences is " << diff <<std::endl;
    //    ros::Duration diff_F = img_Fs.at(1).header.stamp - img_Fs.at(0).header.stamp;
    //    ros::Duration diff_B = img_Bs.at(1).header.stamp - img_Bs.at(0).header.stamp;
    //    ros::Time Temp_F = iv_tStmp.at(0);
    //    ros::Time Temp_B = iv_tStmp.at(0);
    std::string topic_1,topic_2;
    topic_1 = "/ld_image_" + std::to_string(img_Fs.at(0).device_id);
    topic_2 = "/ld_image_" + std::to_string(img_Bs.at(0).device_id);
       for (int i = 0; i<img_Fs.size(); i++){
           write_bag.write(topic_1, img_Fs.at(i).header.stamp + diff_dr , img_Fs.at(i));
       }
    //    std::cout << "end timestamp is " <<  Temp_F << "  end pc timestamp is " << iv_tStmp.at(1499) << std::endl;

       for (int i = 0; i<img_Bs.size(); i++){
           write_bag.write(topic_2, img_Bs.at(i).header.stamp + diff_dr , img_Bs.at(i));
       }
       if (argc == 6){
          img_filepath = argv[currArg++];
          std::vector<ld_msgs::ld_image> imgs;
          std::vector<std::string> img_bagfiles;
          std::vector<std::string> img_bagfilenames;
          file_handler.GetAllFormatFiles(img_filepath, img_bagfiles, ".bag", img_bagfilenames, BAGfilenames);
          for(int i = 0; i<img_bagfiles.size(); i++){
              imgParser imgInfo;
              imgInfo.ImgRead(img_bagfiles.at(i), imgs, T1 , T2);
          }
          if(imgs.size() == 0){
              spdlog::info("pc not match with image bag.");
              continue;
          }
          std::string topic_name;
          topic_name = "/ld_image_" + std::to_string(imgs.at(0).device_id);
          spdlog::info("topic for rosbag image is : {0}", topic_name);
          for(int k = 0; k<imgs.size();k++){
                write_bag.write(topic_name, imgs.at(k).header.stamp, imgs.at(k));
          }
       }

    //    std::cout << "pc 61 is " <<  iv_tStmp.at(61)  << " 62 is " << iv_tStmp.at(62) << std::endl;
    //    std::cout << "can 61 is " << v_can_sync2pc.at(61).header.stamp << " 62 is " << v_can_sync2pc.at(62).header.stamp << std::endl;
    //    std::cout << "msg 61 is " << v_can_sync2pc.at(61).acceleration_x << " 62 is " << v_can_sync2pc.at(62).acceleration_x << std::endl;
       write_bag.close();

        ros_cloud.clear();
        pcl_pointclouds.clear();
        ObjectsList.clear();
        v_can_velocity.clear();
        v_can_yawrate.clear();
        tr_scantimestamp.clear();
        objtr_scantimestamp.clear();
        yaw_ts.clear();
        velo_ts.clear();
        id_sync.push_back(ii);
        img_Fs.clear();
        img_Bs.clear();

    }

    }
    // for(int i = 0 ; i<id_sync.size();i++){
    //     iv_bagfiles.erase(iv_bagfiles.begin()+i);
    //     BAGfilenames.erase(BAGfilenames.begin()+i);
    // }
    // spdlog::info("rest bag sizes is : {0}", iv_bagfiles.size());

    idc_file_.close();

   }
  }
  else if (sync_type == "can"){
      spdlog::info("enter into can sync process..");
      std::vector<std::string>  iv_bagfiles;  // innovusion files (paths) from the
                                         // folder for being processed
      std::vector<std::string>  iv_bagfilenames;  // innovusion files (names) from
                                             // the folder for post-processed
                                             // file names
      std::vector<std::string>  BAGfilenames;
      FileManager file_handler;
      file_handler.GetAllFormatFiles(iv_filepath, iv_bagfiles, ".bag", iv_bagfilenames, BAGfilenames);
      std::sort(iv_bagfiles.begin(),iv_bagfiles.end());
      std::sort(iv_bagfilenames.begin(), iv_bagfilenames.end());
      std::sort(BAGfilenames.begin(), BAGfilenames.end());

      bagAppend bagAdd;

      // paste all can msg into one vector
      std::vector<ld_msgs::ld_can> can_all;
      can_all = bagAdd.canbag_app(idc_filepath);


    for (int iv_idx = 0; iv_idx < iv_bagfiles.size(); iv_idx++) {


    //   std::cout << "iv_idx is " << iv_idx << std::endl;
      rosbag::Bag inno_bag;
      inno_bag.open((std::string)iv_bagfiles.at(iv_idx), rosbag::BagMode::Read);

        rosbag::View iv_view(inno_bag, rosbag::TopicQuery(para_topic.at(0)));
        std::vector<ros::Time> iv_tStmp;
        std::vector<sensor_msgs::PointCloud2> iv_ptClouds;
        foreach(rosbag::MessageInstance const m, iv_view){
        sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
        if(para_topic.at(1) == "header"){
            iv_tStmp.push_back(s->header.stamp);
            iv_ptClouds.push_back(*s);
        }
        else if (para_topic.at(1) == "bag"){
            iv_tStmp.push_back(m.getTime());
            iv_ptClouds.push_back(*s);
        }
    }
    ros::Time T1 = iv_tStmp.front();
    ros::Time T2 = iv_tStmp.back();
    std::cout << "start time: " << T1 << std::endl;

    // --------Step 1----- filter out time-------
    timeRange tFilter;
    std::vector<ld_msgs::ld_can> can_sub;
    bool flag_m;
    flag_m = tFilter.timeFilter(can_all, T1, T2);
    can_sub = tFilter.get_can_sub();
    if(flag_m == false){
       continue;
    }

    //   std::cout << "can sub size is " << can_sub.size() << std::endl;
      // else{

      // std::cout << "can sub size is " << can_sub.size() << std::endl;

      // --------Step 2----- data interpreter, only ld_can
      canInterpreter cData;
      cData.translate(can_sub, id_list, velocity, yaw, accx,accy);
      std::vector<ld_msgs::ld_imugps_can> can_velocity;
      std::vector<ld_msgs::ld_imugps_can> can_yawrate;
      can_velocity = cData.getVelo();
      can_yawrate = cData.getYaw();

      // for(int i = 9 ; i< can_yawrate.size(); i++){
      //     std::cout << "can yawrate is " << can_yawrate.at(i).yawrate << std::endl;
      // }
      // std::cout << "can velocity size is " << can_velocity.size() << std::endl;
      // std::cout << "can yawrate size is " << can_yawrate.size() << std::endl;

      // --------Step 3----- Interpolation
      InterpolationCan iData;
      iData.Interp(iv_tStmp, can_velocity, can_yawrate);
      std::vector<ld_msgs::ld_imugps_can> iCan;
      iCan = iData.get_interpolatedCan();
      int cnt;
      cnt = iData.get_diff();

      // --------Step 4----- merge into one bag file

      if (argc == 6){
          spdlog::info("enter into can/pc/image sync process");
          img_filepath = argv[currArg++];
          std::vector<ld_msgs::ld_image> imgs;
          std::vector<std::string> img_bagfiles;
          std::vector<std::string> img_bagfilenames;
          file_handler.GetAllFormatFiles(img_filepath, img_bagfiles, ".bag", img_bagfilenames, BAGfilenames);

          for(int i = 0; i<img_bagfiles.size(); i++){
              imgParser imgInfo;
              imgInfo.ImgRead(img_bagfiles.at(i), imgs, T1 , T2);
          }
          if(imgs.size() == 0){
              spdlog::info("pc not match with image bag.");
              continue;
          }


        std::string pc_bag = (std::string)iv_bagfiles.at(iv_idx);

        int pos1, pos2;
        pos1 = pc_bag.find_last_of("/");
        iv_ptClouds.erase(iv_ptClouds.begin(), iv_ptClouds.begin() + cnt);
        std::string name = pc_bag.substr(pos1);
        name.pop_back();
        name.pop_back();
        name.pop_back();
        name.pop_back();
        rosbag::Bag bag_sync;
        bag_sync.open(opt_path + name + "_sync_can.bag", rosbag::BagMode::Write);
        for (int idx_iv = 0; idx_iv < iCan.size(); idx_iv++) {
        //    pc_value[idx_iv].header.frame_id = "innovusion";
           bag_sync.write(para_topic.at(0), iv_tStmp.at(idx_iv), iv_ptClouds[idx_iv]);
           bag_sync.write("/ld_imugps_can", iv_tStmp.at(idx_iv), iCan[idx_iv]);
        }


          std::string topic_name;
          topic_name = "/ld_image_" + std::to_string(imgs.at(0).device_id);
          spdlog::info("topic for rosbag image is : {0}", topic_name);
          for(int k = 0; k<imgs.size();k++){
                bag_sync.write(topic_name, imgs.at(k).header.stamp, imgs.at(k));
          }
          iv_ptClouds.clear();
          iCan.clear();
          bag_sync.close();
      }
      else{
      spdlog::info("enter into can/pc sync process");
      bagSync sData;
      sData.write_sync((std::string)iv_bagfiles.at(iv_idx), iv_ptClouds, iv_tStmp, iCan, cnt,
                       opt_path,para_topic.at(0));
      }

    }
  }
  /*
  else if(sync_type == "image"){
          spdlog::info("enter into image section");
          img_filepath = argv[currArg++];
          spdlog::info("img filepath is : {0}", img_filepath);
          std::vector<ld_msgs::ld_image> imgs;
          std::vector<std::string> img_bagfiles;
          std::vector<std::string> img_bagfilenames;
          std::vector<std::string>  iv_bagfiles;
          std::vector<std::string>  iv_bagfilenames;  // innovusion files (names) from
          std::vector<std::string>  BAGfilenames;
          FileManager file_handler;
          file_handler.GetAllFormatFiles(iv_filepath, iv_bagfiles, ".bag", iv_bagfilenames, BAGfilenames);
          std::sort(iv_bagfiles.begin(),iv_bagfiles.end());
          std::sort(iv_bagfilenames.begin(), iv_bagfilenames.end());
          std::sort(BAGfilenames.begin(), BAGfilenames.end());
          file_handler.GetAllFormatFiles(img_filepath, img_bagfiles, ".bag", img_bagfilenames, BAGfilenames);

         for (int iv_idx = 0; iv_idx < iv_bagfiles.size(); iv_idx++) {
            rosbag::Bag inno_bag;
            inno_bag.open((std::string)iv_bagfiles.at(iv_idx), rosbag::BagMode::Read);
            rosbag::View iv_view(inno_bag, rosbag::TopicQuery(para_topic.at(0)));
            std::vector<ros::Time> iv_tStmp;
            std::vector<sensor_msgs::PointCloud2> iv_ptClouds;
            foreach(rosbag::MessageInstance const m, iv_view){
            sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
            if(para_topic.at(1) == "header"){
              iv_tStmp.push_back(s->header.stamp);
              iv_ptClouds.push_back(*s);
            }
            else if (para_topic.at(1) == "bag"){
              iv_tStmp.push_back(m.getTime());
              iv_ptClouds.push_back(*s);
            }
          }
            ros::Time T1 = iv_tStmp.front();
            ros::Time T2 = iv_tStmp.back();
            std::cout << "T1 is  " << T1 << ", T2 is " << T2 << std::endl;

          for(int i = 0; i<img_bagfiles.size(); i++){
              imgParser imgInfo;
              imgInfo.ImgRead(img_bagfiles.at(i), imgs, T1 , T2);
          }
          if(imgs.size() == 0){
              spdlog::info("pc not match with image bag.");
              continue;
          }
          spdlog::info("imgs size is : {0}", imgs.size());
         int pos1, pos2;
         pos1 = iv_bagfiles.at(iv_idx).find_last_of("/");
         std::string name = iv_bagfiles.at(iv_idx).substr(pos1);

         name.pop_back();
         name.pop_back();
         name.pop_back();
         name.pop_back();

         rosbag::Bag write_bag;
         write_bag.open(opt_path + name + "_sync_image.bag", rosbag::BagMode::Write);
         for (int i = 0; i<iv_ptClouds.size() ;i++){
            write_bag.write("/os1_cloud_node/points",iv_tStmp.at(i),iv_ptClouds.at(i));
         }

          std::string topic_name;
          topic_name = "/ld_image_" + std::to_string(imgs.at(0).device_id);
          spdlog::info("topic for rosbag image is : {0}", topic_name);
          for(int k = 0; k<imgs.size();k++){
                write_bag.write(topic_name, imgs.at(k).header.stamp, imgs.at(k));
          }
          write_bag.close();
          iv_tStmp.clear();
          iv_ptClouds.clear();
          imgs.clear();
         }

    }
    */
  //    ivData.bag_demo()

   return 0;
}


// std::string IbeoNtpTime2Str(const ibeo::common::sdk::NTPTime& time) {
//     using boost::posix_time::to_iso_extended_string;
//     std::string ts_str = to_iso_extended_string(time.toPtime());
//     size_t pos = ts_str.find("T");
//     ts_str.replace(pos, 1, " ");
//     while ((pos = ts_str.find("-")) != std::string::npos) {
//         ts_str.replace(pos, 1, "/");
//     }
//     return ts_str;
// }
