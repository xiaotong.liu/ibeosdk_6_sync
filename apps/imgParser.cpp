#include "imgParser.h"
#include <iostream>
#include <fstream>
#include <spdlog/spdlog.h>
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

imgParser::imgParser()
{
    spdlog::info("Image bag sync ... ");
}

void imgParser::ImgRead(std::string img_bag, std::vector<ld_msgs::ld_image>& img_ros, ros::Time T1 , ros::Time T2){
     rosbag::Bag bag;
     bag.open(img_bag, rosbag::BagMode::Read);
     std::vector<std::string> img_topics;
     img_topics.push_back("/ld_image");
     rosbag::View iv_view(bag, rosbag::TopicQuery(img_topics));
     foreach(rosbag::MessageInstance const m, iv_view)
    {
        ld_msgs::ld_image::ConstPtr s = m.instantiate<ld_msgs::ld_image>();
    //   if((int)floor(s->header.stamp.toSec()) <=  floor(T1.toSec()) || s->header.stamp.toSec() >= floor(T2.toSec()) ){  // || s->header.stamp.toSec() >= floor(idc_time.back().toSec())
    //          spdlog::info("Innovusion bag not included in current image bag!!");
    //          break;
    //     }
            // iv_tStmp.push_back(s->header.stamp);
            if(s->header.stamp.toNSec()>=T1.toNSec() && s->header.stamp.toNSec()<=T2.toNSec()){
                img_ros.push_back(*s);
            }
            else{
                spdlog::info("Out of pointcloud time");
                continue;
            }

    }
    bag.close();
}
