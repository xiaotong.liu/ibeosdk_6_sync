#ifndef BASICFUNCS_H
#define BASICFUNCS_H
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

// libs for ibeo
#include <ibeosdk/datablocks/CanMessage.hpp>
#include <ibeosdk/datablocks/PointCloudPlane7510.hpp>
#include <ibeosdk/datablocks/commands/CommandEcuAppBaseCtrl.hpp>
#include <ibeosdk/datablocks/commands/CommandEcuAppBaseStatus.hpp>
#include <ibeosdk/datablocks/commands/EmptyCommandReply.hpp>
#include <ibeosdk/datablocks/commands/ReplyEcuAppBaseStatus.hpp>
#include <ibeosdk/devices/IdcFile.hpp>
#include <ibeosdk/ecu.hpp>
#include <ibeosdk/lux.hpp>
#include <ibeosdk/minilux.hpp>
#include <ibeosdk/scala.hpp>
#include <iomanip>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

// PCL library
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <autoware_msgs/DetectedObject.h>
#include <autoware_msgs/DetectedObjectArray.h>
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOSST_FOREACH

inline std::string int2hex(int i) {
  std::stringstream ioss;
  std::string s_temp;
  ioss << "0x" << std::resetiosflags(std::ios::uppercase) << std::hex << i;
  ioss >> s_temp;
  return s_temp;
}

inline double yawrate_calculate(int& yawrate_vz, double& yawrate_without_vz) {
  switch (yawrate_vz) {
    case 0:
      return yawrate_without_vz;
    case 1:
      return -1 * yawrate_without_vz;
  }
}

inline double kmph2mps(double& kmph) { return kmph * 1000 / 3600; }

inline double degree2radian(double& degree) {
  const double kPi = 3.1415926;
  return degree / 180 * kPi;
}

inline const char* supplementary_sign(const int& sign_num) {
  switch (sign_num) {
    case 0:
      return "None";
    case 1:
      return "Rain";
    case 2:
      return "Snow";
    case 3:
      return "Trailer";
    case 4:
      return "Time";
    case 5:
      return "Arrow_left";
    case 6:
      return "Arrow_right";
    case 7:
      return "BendArrow_left";
    case 8:
      return "BendArrow_right";
    case 9:
      return "Truck";
    case 10:
      return "Distance_arrow";
    case 11:
      return "Weight";
    case 12:
      return "Distance_in";
    case 13:
      return "Tractor";
    case 14:
      return "Snow_rain";
    case 15:
      return "School";
    case 16:
      return "Rain_cloud";
    case 17:
      return "Fog";
    case 18:
      return "Hazardous_materials";
    case 19:
      return "Night";
    case 20:
      return "Supp_sign_generic";
    case 21:
      return "e_rappel";
    case 22:
      return "e_zone";
    case 225:
      return "Invalid_supp";
  }
}

#endif  // BASICFUNCS_H
