/*!
@file bagsync.cpp
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief sync bags
*/

#include "bagsync.h"

bagSync::bagSync() { std::cout << "bag sync starts" << std::endl; }

void bagSync::write_sync(std::string pc_bag,
                         std::vector<sensor_msgs::PointCloud2>& pc_value, std::vector<ros::Time>& pc_time,
                         std::vector<ld_msgs::ld_imugps_can>& iCan, int count,
                         std::string output_path, std::string topic) {
  int pos1, pos2;
  pos1 = pc_bag.find_last_of("/");
  pc_value.erase(pc_value.begin(), pc_value.begin() + count);
  std::string name = pc_bag.substr(pos1);
  // std::string name1 = name.substr(0, name.back() - 4);
  name.pop_back();
  name.pop_back();
  name.pop_back();
  name.pop_back();
  // std::cout << " idc file name is !!!! " << name1 << std::endl;
  rosbag::Bag bag_sync;
  bag_sync.open(output_path + name + "_sync_can.bag", rosbag::BagMode::Write);
  std::cout << "output bag is "
            << std::string(output_path + name + "_sync_can.bag") << std::endl;
  for (int idx_iv = 0; idx_iv < iCan.size(); idx_iv++) {
    //    std::cout << "time stamp" << iCan.at(idx_iv).header.stamp <<
    //    std::endl;

    // pc_value[idx_iv].header.frame_id = "os1_lidar";
    // pc_value[idx_iv].header.stamp = pc_time.at(idx_iv);
    // bag_sync.write("/os1_cloud_node/points", pc_time.at(idx_iv),
    //                pc_value[idx_iv]);
    // iCan[idx_iv].header.stamp = pc_time.at(idx_iv);
    // bag_sync.write("/ld_imugps_can", pc_time.at(idx_iv),
    //                iCan[idx_iv]);

    // pc_value[idx_iv].header.frame_id = "innovusion";
    bag_sync.write(topic, pc_time.at(idx_iv),
                   pc_value[idx_iv]);
    bag_sync.write("/ld_imugps_can", pc_time.at(idx_iv),
                   iCan[idx_iv]);  
    //      if (tunnel.size() > 0) {
    //        bag_write.write("/tunnel_label", pc_time[idx_iv],
    //        *tunnel[idx_iv]);
    //        bag_write.write("/entrance_exit_label", pc_time[idx_iv],
    //        *exit[idx_iv]);
    //        bag_write.write("/adjacent_lane_label", pc_time[idx_iv],
    //        *start[idx_iv]);
    //        bag_write.write("/kitti/velo/pointcloud", pc_time[idx_iv],
    //                        *kitti_pc_value[idx_iv]);
    //        bag_write.write("/points_ground", pc_time[idx_iv],
    //        *gd_pc_value[idx_iv]);
    //        bag_write.write("/lane_points", pc_time[idx_iv],
    //        *lane_pc_value[idx_iv]);
    //      }
  }
  std::cout << "start can msg " << iCan.front().can_ts << " velocity is " 
            << iCan.front().v_x_CAN << ", yawrate is " << iCan.front().yawrate << ", acce is " << iCan.front().acceleration_x <<  std::endl;
  pc_value.clear();
  iCan.clear();
  bag_sync.close();
}
