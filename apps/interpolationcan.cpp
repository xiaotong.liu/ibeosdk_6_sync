#include "interpolationcan.h"

InterpolationCan::InterpolationCan() {}
void InterpolationCan::Interp(std::vector<ros::Time>& pc_time,
                              std::vector<ld_msgs::ld_imugps_can>& can_velocity,
                              std::vector<ld_msgs::ld_imugps_can>& can_yawrate) {
  std::vector<double> time_inno;
  std::vector<ros::Time> finalTime;
  for (int i = 0; i < pc_time.size(); i++) {
    if (pc_time.at(i) > can_velocity.at(0).can_ts &&
        pc_time.at(i) < can_velocity.back().can_ts &&
        pc_time.at(i) > can_yawrate.at(0).can_ts &&
        pc_time.at(i) < can_yawrate.back().can_ts) {
      time_inno.push_back(pc_time.at(i).toSec());
      finalTime.push_back(pc_time.at(i));
    }
  }

  int pc_counter = 0;
  while (time_inno.at(pc_counter) < can_velocity.at(0).can_ts.toSec()) {
    finalTime.erase(finalTime.begin());
    pc_counter++;
  }

  std::vector<double> vec_velocity_fl;
  std::vector<double> vec_yawrate_fl;
  std::vector<double> vec_accx_fl;
  std::vector<double> vec_accy_fl;

  std::vector<double> vec_velocity_fq;
  std::vector<double> vec_yawrate_fq;
  std::vector<double> vec_accx_fq;
  std::vector<double> vec_accy_fq;

  const int size_limit = 2;

  int vel_crt_idx = 0;
  int yaw_crt_idx = 0;

  for (int idx_tIV = 0; idx_tIV < time_inno.size(); idx_tIV++) {
    for (int vel_idx = vel_crt_idx; vel_idx < can_velocity.size() - size_limit;
         vel_idx++) {
      double chk =
          (time_inno.at(idx_tIV) - can_velocity.at(vel_idx).can_ts.toSec()) *
          (time_inno.at(idx_tIV) - can_velocity.at(vel_idx + 1).can_ts.toSec());
      double chk2 =
          (time_inno.at(idx_tIV) - can_velocity.at(vel_idx).can_ts.toSec()) *
          (time_inno.at(idx_tIV) - can_velocity.at(vel_idx + 2).can_ts.toSec());
      if (chk < 0 || chk2 < 0) {
        double lengLeft = fabs(time_inno.at(idx_tIV) -
                               can_velocity.at(vel_idx).can_ts.toSec());
        double lengRight = fabs(time_inno.at(idx_tIV) -
                                can_velocity.at(vel_idx + 1).can_ts.toSec());
        double vel_fl = (can_velocity.at(vel_idx).v_x_CAN * lengRight +
                         can_velocity.at(vel_idx + 1).v_x_CAN * lengLeft) /
                        (lengLeft + lengRight);
        vec_velocity_fl.push_back(vel_fl);

        double delT_01 = can_velocity.at(vel_idx).can_ts.toSec() -
                         can_velocity.at(vel_idx + 1).can_ts.toSec();
        double delT_02 = can_velocity.at(vel_idx).can_ts.toSec() -
                         can_velocity.at(vel_idx + 2).can_ts.toSec();

        double delT_10 = can_velocity.at(vel_idx + 1).can_ts.toSec() -
                         can_velocity.at(vel_idx).can_ts.toSec();
        double delT_12 = can_velocity.at(vel_idx + 1).can_ts.toSec() -
                         can_velocity.at(vel_idx + 2).can_ts.toSec();

        double delT_20 = can_velocity.at(vel_idx + 2).can_ts.toSec() -
                         can_velocity.at(vel_idx).can_ts.toSec();
        double delT_21 = can_velocity.at(vel_idx + 2).can_ts.toSec() -
                         can_velocity.at(vel_idx + 1).can_ts.toSec();

        double delT_g0 =
            time_inno.at(idx_tIV) - can_velocity.at(vel_idx).can_ts.toSec();
        double delT_g1 =
            time_inno.at(idx_tIV) - can_velocity.at(vel_idx + 1).can_ts.toSec();
        double delT_g2 =
            time_inno.at(idx_tIV) - can_velocity.at(vel_idx + 2).can_ts.toSec();

        double vel_fq = can_velocity.at(vel_idx).v_x_CAN * (delT_g1 * delT_g2) /
                            (delT_01 * delT_02) +
                        can_velocity.at(vel_idx + 1).v_x_CAN *
                            (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                        can_velocity.at(vel_idx + 2).v_x_CAN *
                            (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_velocity_fq.push_back(vel_fq);

        vel_crt_idx = vel_idx;
        break;
      }
    }
    for (int yawrate_idx = yaw_crt_idx;
         yawrate_idx < can_yawrate.size() - size_limit; yawrate_idx++) {
      double chk =
          (time_inno.at(idx_tIV) - can_yawrate.at(yawrate_idx).can_ts.toSec()) *
          (time_inno.at(idx_tIV) -
           can_yawrate.at(yawrate_idx + 1).can_ts.toSec());
      double chk2 =
          (time_inno.at(idx_tIV) - can_yawrate.at(yawrate_idx).can_ts.toSec()) *
          (time_inno.at(idx_tIV) -
           can_yawrate.at(yawrate_idx + 2).can_ts.toSec());

      if (chk < 0 || chk2 < 0) {
        double lengLeft = fabs(time_inno.at(idx_tIV) -
                               can_yawrate.at(yawrate_idx).can_ts.toSec());
        double lengRight = fabs(time_inno.at(idx_tIV) -
                                can_yawrate.at(yawrate_idx + 1).can_ts.toSec());
        double yaw_fl = (can_yawrate.at(yawrate_idx).yawrate * lengRight +
                         can_yawrate.at(yawrate_idx + 1).yawrate * lengLeft) /
                        (lengLeft + lengRight);
        vec_yawrate_fl.push_back(yaw_fl);
        double accx_fl =
            (can_yawrate.at(yawrate_idx).acceleration_x * lengRight +
             can_yawrate.at(yawrate_idx + 1).acceleration_x * lengLeft) /
            (lengLeft + lengRight);
        vec_accx_fl.push_back(accx_fl);
        double accy_fl =
            (can_yawrate.at(yawrate_idx).acceleration_y * lengRight +
             can_yawrate.at(yawrate_idx).acceleration_y * lengLeft) /
            (lengLeft + lengRight);
        vec_accy_fl.push_back(accy_fl);

        double delT_01 = can_yawrate.at(yawrate_idx).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx + 1).can_ts.toSec();
        double delT_02 = can_yawrate.at(yawrate_idx).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx + 2).can_ts.toSec();

        double delT_10 = can_yawrate.at(yawrate_idx + 1).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx).can_ts.toSec();
        double delT_12 = can_yawrate.at(yawrate_idx + 1).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx + 2).can_ts.toSec();

        double delT_20 = can_yawrate.at(yawrate_idx + 2).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx).can_ts.toSec();
        double delT_21 = can_yawrate.at(yawrate_idx + 2).can_ts.toSec() -
                         can_yawrate.at(yawrate_idx + 1).can_ts.toSec();

        double delT_g0 =
            time_inno.at(idx_tIV) - can_yawrate.at(yawrate_idx).can_ts.toSec();
        double delT_g1 = time_inno.at(idx_tIV) -
                         can_yawrate.at(yawrate_idx + 1).can_ts.toSec();
        double delT_g2 = time_inno.at(idx_tIV) -
                         can_yawrate.at(yawrate_idx + 2).can_ts.toSec();

        double yawrate_fq = can_yawrate.at(yawrate_idx).yawrate *
                                (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                            can_yawrate.at(yawrate_idx + 1).yawrate *
                                (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                            can_yawrate.at(yawrate_idx + 2).yawrate *
                                (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_yawrate_fq.push_back(yawrate_fq);

        double accx_fq = can_yawrate.at(yawrate_idx).acceleration_x *
                             (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                         can_yawrate.at(yawrate_idx + 1).acceleration_x *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         can_yawrate.at(yawrate_idx + 2).acceleration_x *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accx_fq.push_back(accx_fq);

        double accy_fq = can_yawrate.at(yawrate_idx).acceleration_y *
                             (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                         can_yawrate.at(yawrate_idx + 1).acceleration_y *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         can_yawrate.at(yawrate_idx + 2).acceleration_x *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accy_fq.push_back(accy_fq);

        yaw_crt_idx = yawrate_idx;
        break;
      }
    }
  }

  std::vector<ld_msgs::ld_imugps_can> eng_can;
  for (int idx_inno = 0; idx_inno < finalTime.size(); idx_inno++) {
    ld_msgs::ld_imugps_can temp_eng;
    temp_eng.can_ts = finalTime.at(idx_inno);
    temp_eng.acceleration_x = vec_accx_fl.at(idx_inno);
    temp_eng.acceleration_y = vec_accy_fl.at(idx_inno);
    temp_eng.yawrate = vec_yawrate_fl.at(idx_inno);
    temp_eng.v_x_CAN = vec_velocity_fl.at(idx_inno);
    eng_can.push_back(temp_eng);
  }
  set_interpolatedCan(eng_can);
  set_diff(pc_counter);

  time_inno.clear();
  time_inno.shrink_to_fit();

  vec_velocity_fl.clear();
  vec_velocity_fl.shrink_to_fit();

  vec_yawrate_fl.clear();
  vec_yawrate_fl.shrink_to_fit();

  vec_accx_fl.clear();
  vec_accx_fl.shrink_to_fit();

  vec_accy_fl.clear();
  vec_accy_fl.shrink_to_fit();

  vec_velocity_fq.clear();
  vec_velocity_fq.shrink_to_fit();

  vec_yawrate_fq.clear();
  vec_yawrate_fq.shrink_to_fit();

  vec_accx_fq.clear();
  vec_accx_fq.shrink_to_fit();

  vec_accy_fq.clear();
  vec_accy_fq.shrink_to_fit();
}
