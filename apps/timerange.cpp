/*!
@file timerange.cpp
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief time filter
*/

#include "timerange.h"

timeRange::timeRange() { std::cout << " ---Step 1 start---" << std::endl; }

// std::vector<ld_msgs::ld_can> timeRange::timeFilter(
//    std::vector<ld_msgs::ld_can> can_all, ros::Time T1, ros::Time T2) {
//  std::vector<ld_msgs::ld_can> can_sub;
//  for (int i = 0; i < can_all.size(); i++) {
//    if (can_all.at(i).header.stamp >= T1 && can_all.at(i).header.stamp <= T2)
//    {
//      can_sub.push_back(can_all.at(i));
//    }
//    // else {
//    //      std::cout << "innovusion bag and can ban donot match each other at
//    //      all"
//    //                << std::endl;
//    //    }
//  }
//  if (can_sub.size() == 0) {
//    std::cout << "innovusion and can bag donot match each other at all."
//              << std::endl;
//    //    can_sub.push_back(can_all.at(0));
//    //    can_sub.push_back(can_all.at(1));
//    //    can_sub.push_back(can_all.at(2));
//  }
//  return can_sub;
//}
bool timeRange::timeFilter(
    std::vector<ld_msgs::ld_can>& can_all, ros::Time T1, ros::Time T2) {
  std::vector<ld_msgs::ld_can> can_sub;
  int t1 = (int)floor(T1.toSec()) - 5;
  ros::Time test_time_1(static_cast<ros::Time>(t1));
  int t2 = (int)floor(T2.toSec()) + 5;
  ros::Time test_time_2(static_cast<ros::Time>(t2));
  for (int i = 0; i < can_all.size(); i++) {
    if (can_all.at(i).header.stamp >= test_time_1 &&
        can_all.at(i).header.stamp <= test_time_2) {
      can_sub.push_back(can_all.at(i));
    }
    // else {
    //      std::cout << "innovusion bag and can ban donot match each other at
    //      all"
    //                << std::endl;
    //    }
   
  }
  if (can_sub.size() == 0) {
    std::cout << "innovusion and can bag donot match each other at all."
              << std::endl;
    return false;
    //    can_sub.push_back(can_all.at(0));
    //    can_sub.push_back(can_all.at(1));
    //    can_sub.push_back(can_all.at(2));
  }
  set_can_sub(can_sub);
  return true;
}
