/*!
@file timerange.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief time filter
*/
#ifndef TIMERANGE_H
#define TIMERANGE_H
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <iostream>
#include <string>
#include <vector>

class timeRange {
public:
  timeRange();
  bool timeFilter(std::vector<ld_msgs::ld_can>& can_all,
                                          ros::Time T1, ros::Time T2);
  void set_can_sub(std::vector<ld_msgs::ld_can> can_sub) {m_can = can_sub;}
  std::vector<ld_msgs::ld_can> get_can_sub() {return m_can;}

private:
  std::vector<ld_msgs::ld_can> m_can;
};

#endif  // TIMERANGE_H
