/*!
@file bagappend.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief add up all can bags in folder
*/
#ifndef BAGAPPEND_H  // NOLINT
#define BAGAPPEND_H

// libs for rosbag API
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <string>
#include <vector>
#include "std_msgs/String.h"

class bagAppend {
 public:
  bagAppend();
  std::vector<ld_msgs::ld_can> canbag_app(const std::string& filepath);
};

#endif  // BAGAPPEND_H
