/*!
@file interpolation.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief 2nd order interpolation
*/

#ifndef INTERPOLATIONCAN_H
#define INTERPOLATIONCAN_H
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <bitset>
#include <iostream>
#include <string>
#include <vector>

class InterpolationCan {
 public:
  InterpolationCan();
  void Interp(std::vector<ros::Time>& pc_time,
              std::vector<ld_msgs::ld_imugps_can>& can_velocity,
              std::vector<ld_msgs::ld_imugps_can>& can_yawrate);
  void set_interpolatedCan(std::vector<ld_msgs::ld_imugps_can>& eng_can) {
    m_eng_can = eng_can;
  }
  std::vector<ld_msgs::ld_imugps_can> get_interpolatedCan() {
    return m_eng_can;
  }
  void set_diff(int cnt) { m_cnt = cnt; }
  int get_diff() { return m_cnt; }

 private:
  std::vector<ld_msgs::ld_imugps_can> m_eng_can;
  int m_cnt;
};

#endif  // INTERPOLATIONCAN_H
