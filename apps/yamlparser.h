#ifndef YAMLPARSER_H
#define YAMLPARSER_H
#include <yaml-cpp/yaml.h>
#include <iostream>

struct transVector {
  double x;
  double y;
  double z;
  double yaw;
  double pitch;
  double roll;
};
struct coorsVector{
  double X;
  double Y;
  double Z;
  double qw;
  double qx;
  double qy;
  double qz;
};

class yamlParser {
 public:
  yamlParser();
  void set_trans_vec(transVector vec) { m_vec = vec; }
  transVector get_trans_vec() { return m_vec; }
  void set_coor_vec(coorsVector co) { m_co = co; }
  coorsVector get_coor_vec() { return m_co; }
 private:
  transVector m_vec;
  coorsVector m_co;
};

#endif  // YAMLPARSER_H
