/*!
@file caninterpreter.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief parser can information header file
*/
// #ifndef CANINTERPRETER_H
// #define CANINTERPRETER_H

#ifndef CANINTERPRETER_H
#define CANINTERPRETER_H

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include <ld_can/can_interpreter.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <bitset>
#include <iostream>
#include <vector>

class canInterpreter {
 public:
  canInterpreter();
  void translate(std::vector<ld_msgs::ld_can>& can_sub, std::vector<int>& id_list,ld::CanInterpreter<double>& velocity, ld::CanInterpreter<double>& yaw, ld::CanInterpreter<double>& accx, ld::CanInterpreter<double>& accy);
  void setVelo(std::vector<ld_msgs::ld_imugps_can>& can_velo) {
    m_can_velo = can_velo;
  }
  std::vector<ld_msgs::ld_imugps_can> getVelo() { return m_can_velo; }
  void setYaw(std::vector<ld_msgs::ld_imugps_can>& can_yawrate) {
    m_can_yawrate = can_yawrate;
  }
  std::vector<ld_msgs::ld_imugps_can> getYaw() { return m_can_yawrate; }

 private:
  std::vector<ld_msgs::ld_imugps_can> m_can_velo;
  std::vector<ld_msgs::ld_imugps_can> m_can_yawrate;
};

#endif  // CANINTERPRETER_H
