#include "filemanager.h"

FileManager::FileManager() {}

// void FileManager::GetAllFormatFiles(std::string path,
// std::vector<std::string>& files, std::string format,
//                                    std::vector<std::string>&
//                                    output_filenames,
//                                    std::vector<std::string>& BAGfilenames)
// void FileManager::GetAllFormatFiles(
//    std::string path, std::vector<std::string>& files, std::string format,
//    std::vector<std::string>& output_filenames) {
//  std::vector<std::string> BAGfilenames;
//  // path = path.append("/");

//  DIR* p_dir;
//  const char* str = path.c_str();
//  std::cout << "input path is " << str << std::endl;
//  p_dir = opendir(str);
//  if (p_dir == NULL) {
//    std::cout << "can't open :" << path << std::endl;
//  }
//  struct dirent* p_dirent;
//  while (p_dirent = readdir(p_dir)) {
//    std::string tmpFileName = p_dirent->d_name;
//    std::cout << "temp file name is " << tmpFileName << std::endl;
//    if (tmpFileName.find(format) != std::string::npos) {
//      output_filenames.push_back(tmpFileName);
//      files.push_back(path + "/" + tmpFileName);
//      if (format == ".bag") {
//        BAGfilenames.push_back(tmpFileName);
//      }
//    }
//  }
//  closedir(p_dir);
//}
void FileManager::GetAllFormatFiles(std::string path,
                                    std::vector<std::string>& files,
                                    std::string format,
                                    std::vector<std::string>& output_filenames,
                                    std::vector<std::string>& BAGfilenames) {
  // path = path.append("/");
  DIR* p_dir;
  const char* str = path.c_str();
  p_dir = opendir(str);
  if (p_dir == NULL) {
    std::cout << "can't open :" << path << std::endl;
  }
  struct dirent* p_dirent;
  while (p_dirent = readdir(p_dir)) {
    std::string tmpFileName = p_dirent->d_name;
    //    std::cout << "temp file name is " << tmpFileName << std::endl;
    if (tmpFileName.find(format) != std::string::npos) {
      output_filenames.push_back(tmpFileName);
      files.push_back(path + "/" + tmpFileName);
      if (format == ".bag") {
        BAGfilenames.push_back(tmpFileName);
      }
    }
  }
  std::sort(files.begin(),files.end());
  // std::sort(BAGfilenames.begin(),BAGfilenames.end());
  std::sort(output_filenames.begin(),output_filenames.end());
  closedir(p_dir);
}

void FileManager::FileNamesSort(std::vector<std::string>& filenames) {
  std::sort(filenames.begin(), filenames.end());
  return;
}

std::vector<std::string> FileManager::GetAllBagFiles(std::string path,
                                                     std::string format) {
  // path = path.append("/");
  std::vector<std::string> files;
  std::vector<std::string> output_filenames;
  std::vector<std::string> BAGfilenames;
  DIR* p_dir;
  const char* str = path.c_str();
  p_dir = opendir(str);
  if (p_dir == NULL) {
    std::cout << "can't open :" << path << std::endl;
  }
  struct dirent* p_dirent;
  while (p_dirent = readdir(p_dir)) {
    std::string tmpFileName = p_dirent->d_name;
    if (tmpFileName.find(format) != std::string::npos) {
      output_filenames.push_back(tmpFileName);
      files.push_back(path + "/" + tmpFileName);
      if (format == ".bag") {
        BAGfilenames.push_back(tmpFileName);
      } else {
        BAGfilenames.push_back("0");
      }
    }
    std::sort(output_filenames.begin(), output_filenames.end());
    std::sort(files.begin(), files.end());
  }
  closedir(p_dir);
  return files;
}
