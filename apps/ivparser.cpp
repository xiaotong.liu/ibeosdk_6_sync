#include "ivparser.h"
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

ivParser::ivParser()
{
    
}

void ivParser::bag_demo(const std::string& filename)
{
    std::vector<ros::Time> iv_tStmp;
    rosbag::Bag iv_bag;
    iv_bag.open(filename, rosbag::bagmode::Read);
    std::vector<std::string> iv_topics;
    iv_topics.push_back("/iv_points");
    rosbag::View iv_view(iv_bag, rosbag::TopicQuery(iv_topics));
    std::vector<sensor_msgs::PointCloud2> iv_ptClouds;
    foreach(rosbag::MessageInstance const m, iv_view)
    {
        sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
        iv_tStmp.push_back(s->header.stamp);
        iv_ptClouds.push_back(*s);
    }
    set_ivpc(iv_ptClouds);
    set_ivpc_ts(iv_tStmp);
    iv_bag.close();
    // std::cout << " inside ivParser, innovusion point cloud size is " << iv_ptClouds.size() << std::endl;
}
