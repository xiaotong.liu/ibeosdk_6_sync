#ifndef IVPARSER_H
#define IVPARSER_H

// libs for rosbag API
#include <iostream>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"

class ivParser
{
public:
    ivParser();
    void set_ivpc(std::vector<sensor_msgs::PointCloud2> ivpc) { m_ivpc = ivpc;}
    std::vector<sensor_msgs::PointCloud2> get_ivpc() {return m_ivpc;}
    void set_ivpc_ts(std::vector<ros::Time> ivpc_ts) {m_ivpc_ts = ivpc_ts;}
    std::vector<ros::Time> get_ivpc_ts() {return m_ivpc_ts;}
    void bag_demo(const std::string& filename);

private:
    std::vector<sensor_msgs::PointCloud2> m_ivpc;
    std::vector<ros::Time> m_ivpc_ts;

};

#endif // IVPARSER_H
