
/*!
@file caninterpreter.cpp
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief parser can information
*/

#include "caninterpreter.h"

canInterpreter::canInterpreter() {
  std::cout << "interpreter starts!" << std::endl;
}
double kmph2mps(const double& kmph) { return kmph * 1000 / 3600; }

double degree2radian(const double& degree) {
  const double kPi = 3.1415926;
  return degree / 180 * kPi;
}

double yawrate_calculate(const int& yawrate_vz,
                         const double& yawrate_without_vz) {
  switch (yawrate_vz) {
    case 0:
      return yawrate_without_vz;
    case 1:
      return -1 * yawrate_without_vz;
  }
}

void canInterpreter::translate(std::vector<ld_msgs::ld_can>& can_sub, std::vector<int>& id_list, ld::CanInterpreter<double>& velocity, ld::CanInterpreter<double>& yaw, ld::CanInterpreter<double>& accx, ld::CanInterpreter<double>& accy) {

  std::vector<ld_msgs::ld_imugps_can> can_velocity;   // passat
  std::vector<ld_msgs::ld_imugps_can> can_yawrate;

  uint8_t data[8];
  
  for (int i = 0; i < can_sub.size(); i++) {
  data[0] = static_cast<int>(can_sub.at(i).DATA[0]);
  data[1] = static_cast<int>(can_sub.at(i).DATA[1]);
  data[2] = static_cast<int>(can_sub.at(i).DATA[2]);
  data[3] = static_cast<int>(can_sub.at(i).DATA[3]);
  data[4] = static_cast<int>(can_sub.at(i).DATA[4]);
  data[5] = static_cast<int>(can_sub.at(i).DATA[5]);
  data[6] = static_cast<int>(can_sub.at(i).DATA[6]);
  data[7] = static_cast<int>(can_sub.at(i).DATA[7]); 
  int len = *(&data + 1) - data;
  ld_msgs::ld_imugps_can temp_velocity;
  ld_msgs::ld_imugps_can temp_yawrate;
  const double kPi=3.1415926;

  if (static_cast<int>(can_sub.at(i).ID) == id_list.at(0)){
      temp_velocity.v_x_CAN = velocity.can_parser(data, len);
      // std::cout << "velocity is : " << temp_velocity.v_x_CAN << std::endl;
      temp_velocity.can_ts = can_sub.at(i).header.stamp;
      temp_velocity.header.frame_id = can_sub.at(i).header.frame_id;
      can_velocity.push_back(temp_velocity);
  }
  if (static_cast<int>(can_sub.at(i).ID) == id_list.at(1)){
      temp_yawrate.can_ts = can_sub.at(i).header.stamp;
      temp_yawrate.header.frame_id = can_sub.at(i).header.frame_id;
      temp_yawrate.acceleration_x = accx.can_parser(data, len);
      temp_yawrate.acceleration_y = accy.can_parser(data, len);
      temp_yawrate.yawrate = yaw.can_parser(data,len) /180*kPi;
      // std::cout << "yawrate is : " << temp_yawrate.yawrate << std::endl;
      can_yawrate.push_back(temp_yawrate);
  }
  }

  /*
  if (vehicle_type == "passat"){
  int data[8];
  double velocity_raw;
  double velocity_kmh;
  double velocity_ms;
  double yawrate_raw;
  double yawrate_without_vz;
  int yawrate_vz;
  double yawrate_degree;
  double yawrate_radian;
  double acc_x_raw;
  double acc_x;
  double acc_y_raw;
  double acc_y;
  const double kGravi = 9.8;
  for (int i = 0; i < can_sub.size(); i++) {
    data[0] = static_cast<int>(can_sub.at(i).DATA[0]);
    data[1] = static_cast<int>(can_sub.at(i).DATA[1]);
    data[2] = static_cast<int>(can_sub.at(i).DATA[2]);
    data[3] = static_cast<int>(can_sub.at(i).DATA[3]);
    data[4] = static_cast<int>(can_sub.at(i).DATA[4]);
    data[5] = static_cast<int>(can_sub.at(i).DATA[5]);
    data[6] = static_cast<int>(can_sub.at(i).DATA[6]);
    data[7] = static_cast<int>(can_sub.at(i).DATA[7]);
    std::bitset<8> byte0(data[0]);
    std::bitset<8> byte1(data[1]);
    std::bitset<8> byte2(data[2]);
    std::bitset<8> byte3(data[3]);
    std::bitset<8> byte4(data[4]);
    std::bitset<8> byte5(data[5]);
    std::bitset<8> byte6(data[6]);
    std::bitset<8> byte7(data[7]);
    ld_msgs::ld_imugps_can temp_velocity;
    ld_msgs::ld_imugps_can temp_yawrate;
    if (static_cast<int>(can_sub.at(i).ID) == 253) {
      velocity_raw = (data[5] << 8 | data[4]);
      velocity_kmh = velocity_raw * 0.01;
      velocity_ms = kmph2mps(velocity_kmh);
      temp_velocity.v_x_CAN = velocity_ms;
      temp_velocity.can_ts = can_sub.at(i).header.stamp;
      temp_velocity.header.frame_id = can_sub.at(i).header.frame_id;
      can_velocity.push_back(temp_velocity);
    }
    if (static_cast<int>(can_sub.at(i).ID) == 257) {
      yawrate_raw = (data[6] & 0x3f) << 8 | data[5];
      yawrate_without_vz = yawrate_raw * 0.01;
      yawrate_vz = (data[6] & 0x40) >> 6;
      yawrate_degree = yawrate_calculate(yawrate_vz, yawrate_without_vz);
      yawrate_radian = degree2radian(yawrate_degree);
      acc_x_raw = (data[4] & 0x3) << 8 | data[3];
      acc_x = acc_x_raw * 0.03125 - 16;
      acc_y_raw = data[2];
      acc_y = (acc_y_raw * 0.01 - 1.27) * kGravi;
      temp_yawrate.can_ts = can_sub.at(i).header.stamp;
      temp_yawrate.header.frame_id = can_sub.at(i).header.frame_id;
      temp_yawrate.acceleration_x = acc_x;
      temp_yawrate.acceleration_y = acc_y;
      temp_yawrate.yawrate = yawrate_radian;
      can_yawrate.push_back(temp_yawrate);
    }
  }
  }
  else if (vehicle_type == "haval"){
    int data[8];
  double velocity_raw;
  double velocity_kmh;
  double velocity_ms;
  double yawrate_raw;
  double yawrate_without_vz;
  int yawrate_vz;
  double yawrate_degree;
  double yawrate_radian;
  double acc_x_raw;
  double acc_x;
  double acc_y_raw;
  double acc_y;
  const double kGravi = 9.8;
  for (int i = 0; i < can_sub.size(); i++) {
    data[0] = static_cast<int>(can_sub.at(i).DATA[0]);
    data[1] = static_cast<int>(can_sub.at(i).DATA[1]);
    data[2] = static_cast<int>(can_sub.at(i).DATA[2]);
    data[3] = static_cast<int>(can_sub.at(i).DATA[3]);
    data[4] = static_cast<int>(can_sub.at(i).DATA[4]);
    data[5] = static_cast<int>(can_sub.at(i).DATA[5]);
    data[6] = static_cast<int>(can_sub.at(i).DATA[6]);
    data[7] = static_cast<int>(can_sub.at(i).DATA[7]);
    std::bitset<8> byte0(data[0]);
    std::bitset<8> byte1(data[1]);
    std::bitset<8> byte2(data[2]);
    std::bitset<8> byte3(data[3]);
    std::bitset<8> byte4(data[4]);
    std::bitset<8> byte5(data[5]);
    std::bitset<8> byte6(data[6]);
    std::bitset<8> byte7(data[7]);
    ld_msgs::ld_imugps_can temp_velocity;
    ld_msgs::ld_imugps_can temp_yawrate;
    if (static_cast<int>(can_sub.at(i).ID) == 613) {//speed change co 613
     
     //##################velocity_kmh
     unsigned long velocity_kmh_raw=(byte0<<3>>3).to_ulong()*256+byte1.to_ulong();
     double velocity_kmh=velocity_kmh_raw*0.05625+0.0;
      //##################

      //velocity_raw = (data[5] << 8 | data[4]);
      //velocity_kmh = velocity_raw * 0.01;
      velocity_ms = kmph2mps(velocity_kmh);
      temp_velocity.v_x_CAN = velocity_ms;
      temp_velocity.can_ts = can_sub.at(i).header.stamp;
      temp_velocity.header.frame_id = can_sub.at(i).header.frame_id;
      can_velocity.push_back(temp_velocity);
    }
    if (static_cast<int>(can_sub.at(i).ID) == 581) {//change from 257 to 581
      
      //##################yawrate_raw
      unsigned long  yawrate_radian_raw=byte4.to_ulong()*256+byte5.to_ulong();
      double yawrate_radian=yawrate_radian_raw*0.00024-2.093;
      //##################

      //##################acc_x_raw
     unsigned long acc_x_raww=byte0.to_ulong()*256+byte1.to_ulong();
     double acc_x=acc_x_raww*0.00098-21.592;
     //##################

     //##################acc_y_raw
     unsigned long acc_y_raww=byte2.to_ulong()*256+byte3.to_ulong();
     double acc_y=acc_y_raww*0.00098-21.592;
     //##################

      yawrate_raw = (data[6] & 0x3f) << 8 | data[5];
      yawrate_without_vz = yawrate_raw * 0.01;

      yawrate_vz = (data[6] & 0x40) >> 6;
      yawrate_degree = yawrate_calculate(yawrate_vz, yawrate_without_vz);
      //yawrate_radian = degree2radian(yawrate_degree);

      //acc_x_raw = (data[4] & 0x3) << 8 | data[3];
      //acc_x = acc_x_raw * 0.03125 - 16;
      //acc_y_raw = data[2];
      //acc_y = (acc_y_raw * 0.01 - 1.27) * kGravi;
      temp_yawrate.can_ts = can_sub.at(i).header.stamp;
      temp_yawrate.header.frame_id = can_sub.at(i).header.frame_id;
      temp_yawrate.acceleration_x = acc_x;
      temp_yawrate.acceleration_y = acc_y;
      temp_yawrate.yawrate = yawrate_radian;
      can_yawrate.push_back(temp_yawrate);
    }
 
  }
  } */
  setVelo(can_velocity);
  setYaw(can_yawrate);

/*
  // Golf
   std::vector<ld_msgs::ld_imugps_can> can_velocity;
  std::vector<ld_msgs::ld_imugps_can> can_yawrate;
  int data[8];
  double velocity_raw;
  double velocity_kmh;
  double velocity_ms;
  double yawrate_raw;
  double yawrate_without_vz;
  int yawrate_vz;
  double yawrate_degree;
  double yawrate_radian;
  double acc_x_raw;
  double acc_x;
  double acc_y_raw;
  double acc_y;
  const double kGravi = 9.8;
  for (int i = 0; i < can_sub.size(); i++) {
    data[0] = static_cast<int>(can_sub.at(i).DATA[0]);
    data[1] = static_cast<int>(can_sub.at(i).DATA[1]);
    data[2] = static_cast<int>(can_sub.at(i).DATA[2]);
    data[3] = static_cast<int>(can_sub.at(i).DATA[3]);
    data[4] = static_cast<int>(can_sub.at(i).DATA[4]);
    data[5] = static_cast<int>(can_sub.at(i).DATA[5]);
    data[6] = static_cast<int>(can_sub.at(i).DATA[6]);
    data[7] = static_cast<int>(can_sub.at(i).DATA[7]);
    std::bitset<8> byte0(data[0]);
    std::bitset<8> byte1(data[1]);
    std::bitset<8> byte2(data[2]);
    std::bitset<8> byte3(data[3]);
    std::bitset<8> byte4(data[4]);
    std::bitset<8> byte5(data[5]);
    std::bitset<8> byte6(data[6]);
    std::bitset<8> byte7(data[7]);
    ld_msgs::ld_imugps_can temp_velocity;
    ld_msgs::ld_imugps_can temp_yawrate;
    if (static_cast<int>(can_sub.at(i).ID) == 613) {//speed change co 613
     
     //##################velocity_kmh
     unsigned long velocity_kmh_raw=(byte0<<3>>3).to_ulong()*256+byte1.to_ulong();
     double velocity_kmh=velocity_kmh_raw*0.05625+0.0;
      //##################

      //velocity_raw = (data[5] << 8 | data[4]);
      //velocity_kmh = velocity_raw * 0.01;
      velocity_ms = kmph2mps(velocity_kmh);
      temp_velocity.v_x_CAN = velocity_ms;
      temp_velocity.can_ts = can_sub.at(i).header.stamp;
      temp_velocity.header.frame_id = can_sub.at(i).header.frame_id;
      can_velocity.push_back(temp_velocity);
    }
    if (static_cast<int>(can_sub.at(i).ID) == 581) {//change from 257 to 581
      
      //##################yawrate_raw
      unsigned long  yawrate_radian_raw=byte4.to_ulong()*256+byte5.to_ulong();
      double yawrate_radian=yawrate_radian_raw*0.00024-2.093;
      //##################

      //##################acc_x_raw
     unsigned long acc_x_raww=byte0.to_ulong()*256+byte1.to_ulong();
     double acc_x=acc_x_raww*0.00098-21.592;
     //##################

     //##################acc_y_raw
     unsigned long acc_y_raww=byte2.to_ulong()*256+byte3.to_ulong();
     double acc_y=acc_y_raww*0.00098-21.592;
     //##################

      yawrate_raw = (data[6] & 0x3f) << 8 | data[5];
      yawrate_without_vz = yawrate_raw * 0.01;

      yawrate_vz = (data[6] & 0x40) >> 6;
      yawrate_degree = yawrate_calculate(yawrate_vz, yawrate_without_vz);
      //yawrate_radian = degree2radian(yawrate_degree);

      //acc_x_raw = (data[4] & 0x3) << 8 | data[3];
      //acc_x = acc_x_raw * 0.03125 - 16;
      //acc_y_raw = data[2];
      //acc_y = (acc_y_raw * 0.01 - 1.27) * kGravi;
      temp_yawrate.can_ts = can_sub.at(i).header.stamp;
      temp_yawrate.header.frame_id = can_sub.at(i).header.frame_id;
      temp_yawrate.acceleration_x = acc_x;
      temp_yawrate.acceleration_y = acc_y;
      temp_yawrate.yawrate = yawrate_radian;
      can_yawrate.push_back(temp_yawrate);
    }
 
  }
  std::cout << "for loop finished" << std::endl;
  setVelo(can_velocity);
  setYaw(can_yawrate);
  */
}
