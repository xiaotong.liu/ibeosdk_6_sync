#ifndef SYNC_IDC_H
#define SYNC_IDC_H



// #include "alllistener.h"
#include "transform_coordinates.h"
#include "interpolation.h"
// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"

// PCL library
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <autoware_msgs/DetectedObject.h>
#include <autoware_msgs/DetectedObjectArray.h>
#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>
#include "spdlog/spdlog.h"

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH


bool syn_everything(const std::string& iv_path, const std::string& idc_path, const std::string& opt_path,
                    std::string iv_filename, std::vector<float>& paras, bool is_paras_given)
{



    if (is_paras_given)
    {
        std::cout << "Parameters are given." << std::endl;
    }

    else
    {
        std::cout << "No parameters is given." << std::endl;
    }

    std::cout << "Start loading bag files" << std::endl;
    rosbag::Bag idc_bag;
    rosbag::Bag iv_bag;
    idc_bag.open(idc_path, rosbag::bagmode::Read);
    iv_bag.open(iv_path, rosbag::bagmode::Read);

    std::vector<std::string> idc_topics;
    std::vector<std::string> iv_topics;


    /* above newly added on 10.09.2019*/
    idc_topics.push_back(std::string("/lux_yawrate"));
    idc_topics.push_back(std::string("/lux_velocity"));
    /* above newly added on 30.08.2019*/
    idc_topics.push_back(std::string("/lux_pointcloud"));
    idc_topics.push_back(std::string("/lux_ObjectList_no"));
    idc_topics.push_back(std::string("/lux_ObjectList_yes"));
    iv_topics.push_back(std::string("/iv_points"));

    rosbag::View idc_view(idc_bag, rosbag::TopicQuery(idc_topics));
    rosbag::View iv_view(iv_bag, rosbag::TopicQuery(iv_topics));


//    std::vector<ld_data::ObjectList2010> idc_Objlist_ld_NO;
//    std::vector<ld_data::ObjectList2010> idc_Objlist_ld_YES;
    /* above newly added on 10.09.2019*/
    std::vector<ld_msgs::ld_imugps_can> v_can_velocity;
    std::vector<ld_msgs::ld_imugps_can> v_can_yawrate;
    std::vector<ros::Time> idc_can_ts;
    /* above newly added on 30.08.2019*/
    std::vector<ros::Time> idc_tStmp;
    std::vector<sensor_msgs::PointCloud2::ConstPtr> idc_ptClouds;
    std::vector<autoware_msgs::DetectedObjectArray::ConstPtr> idc_Objlist_NO;
    std::vector<autoware_msgs::DetectedObjectArray::ConstPtr> idc_Objlist_YES;



    std::vector<ros::Time> iv_tStmp;
    std::vector<sensor_msgs::PointCloud2::ConstPtr> iv_ptClouds;


    foreach(rosbag::MessageInstance const m, iv_view)
    {
        sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();

        iv_tStmp.push_back(s->header.stamp);
        iv_ptClouds.push_back(s);
    }

    iv_bag.close();

    spdlog::info("Finish loading bag files.");

    foreach(rosbag::MessageInstance const m, idc_view)
    {
        sensor_msgs::PointCloud2::ConstPtr s = m.instantiate<sensor_msgs::PointCloud2>();
        autoware_msgs::DetectedObjectArray::ConstPtr t = m.instantiate<autoware_msgs::DetectedObjectArray>();

        ld_msgs::ld_imugps_can::ConstPtr u = m.instantiate<ld_msgs::ld_imugps_can>();

        if (m.getTopic() == "/lux_ObjectList_yes")
        {
            idc_Objlist_YES.push_back(t);
        }
        if (m.getTopic() == "/lux_ObjectList_no")
        {
            idc_Objlist_NO.push_back(t);
        }
        if (m.getTopic() == "/lux_pointcloud")
        {
            idc_tStmp.push_back(s->header.stamp);
            idc_ptClouds.push_back(s);
        }
        // if (m.getTopic() == "/ld_can")
        // {
        //     idc_can_ts.push_back(u->header.stamp);
        //     idc_can.push_back(u);
        // }
        if (m.getTopic() == "/lux_yawrate")
        {
            // idc_tStmp.push_back(s->header.stamp);
            v_can_yawrate.push_back(*u);
        }
        if (m.getTopic() == "/lux_velocity")
        {
            // idc_tStmp.push_back(s->header.stamp);
            v_can_velocity.push_back(*u);
        }
    }

    idc_bag.close();


    const int boundary_limit = 5;

    int idc_v_start_time = (int)floor(v_can_velocity.front().can_ts.toSec());
    int idc_v_end_time = (int)floor(v_can_velocity.back().can_ts.toSec());
    int idc_yw_start_time = (int)floor(v_can_yawrate.front().can_ts.toSec());
    int idc_yw_end_time = (int)floor(v_can_yawrate.back().can_ts.toSec());

    int iv_start_time = (int)floor(iv_tStmp.front().toSec())-boundary_limit;
    int iv_end_time = (int)floor(iv_tStmp.back().toSec())+boundary_limit;


    if (iv_start_time <= idc_v_start_time || iv_end_time >= idc_v_end_time || iv_start_time <= idc_yw_start_time || iv_end_time >= idc_yw_end_time )

    {
        std::cout << iv_filename << " seems an invalid point cloud file." << std::endl;
        return false;
    }


    /*************Finish loading bag files********************/


    /*************Writing synchronized bag files********************/
    std::cout << "Start synchronizing process " << std::endl;

    /******Interpolation begins*******/


    interpolation itrpl_quadratic(iv_tStmp, v_can_velocity, v_can_yawrate);


    /******Interpolation ends*******/


    /* for combining velocity and yawrate into a message */
    ld_msgs::ld_imugps_can can_sync2pc;

    /* vector for gathering interpolated and synchronized CAN message */
    std::vector<ld_msgs::ld_imugps_can> v_can_sync2pc;

    /* fill up the vector in order to write bag file with everything */
    for (int idx_inno = 0; idx_inno < iv_tStmp.size(); idx_inno++)
    {
        //		can_write_yawrate_acc.acceleration_x = vec_accx_fl.at(idx_inno);
        //		can_write_yawrate_acc.acceleration_y = vec_accy_fl.at(idx_inno);
        //		can_write_yawrate_acc.yawrate = vec_yawrate_fl.at(idx_inno);
        //		can_write_yawrate_acc.v_x_CAN = vec_velocity_fl.at(idx_inno);

        can_sync2pc.acceleration_x = itrpl_quadratic.get_vec_accx_fq().at(idx_inno);
        can_sync2pc.acceleration_y = itrpl_quadratic.get_vec_accy_fq().at(idx_inno);
        can_sync2pc.yawrate = itrpl_quadratic.get_vec_yawrate_fq().at(idx_inno);
        can_sync2pc.v_x_CAN = itrpl_quadratic.get_vec_velocity_fq().at(idx_inno);
        can_sync2pc.header.stamp = iv_tStmp.at(idx_inno);
        v_can_sync2pc.push_back(can_sync2pc);
        //		std::cout << finalTime.at(idx_inno) << std::endl;
    }

    rosbag::Bag write_bag;

    const int dot_pos = iv_filename.find(".");

    std::string iv_name_tobe_added = iv_filename.substr(0, dot_pos);


    const std::string sync_file = opt_path +"/"+ iv_name_tobe_added +"_sync.bag";
    write_bag.open(sync_file, rosbag::bagmode::Write);


    std::vector<ros::Time> idc2ivTime;
    std::vector<sensor_msgs::PointCloud2::ConstPtr> idc2iv_ptClouds;
    std::vector<autoware_msgs::DetectedObjectArray::ConstPtr> idc2iv_Objlist_NO;
    std::vector<autoware_msgs::DetectedObjectArray::ConstPtr> idc2iv_Objlist_YES;

    std::vector<ld_msgs::ld_can::ConstPtr> idc2iv_can;



    // Synchronize idc time stamps(object lists, point cloud) to innovusion ones (point cloud)
    int idc2iv_Idx = 0;
    int idx_idc2iv_can = 0;

    for (unsigned int idx_iv = 0; idx_iv < iv_tStmp.size(); idx_iv++)
    {

        ros::Duration minTD = iv_tStmp.at(idx_iv) - idc_tStmp.at(0);

        // Run through the idc time stamps and find the smallest time difference
        for (int idx_idc = 0; idx_idc < idc_tStmp.size(); ++idx_idc)
        {
            ros::Duration crtTD = iv_tStmp.at(idx_iv) - idc_tStmp.at(idx_idc);
            if (crtTD < minTD && crtTD > ros::Duration(0.0))
            {
                minTD = crtTD;
                idc2iv_Idx = idx_idc;
            }
        }
        //        for (int idx_idc_can = 0; idx_idc_can < idc_can_ts.size(); ++idx_idc_can)
        //        {
        //            ros::Duration crtTD = idc_can_ts.at(idx_iv) - idc_can_ts.at(idx_idc_can);
        //            if (crtTD < minTD && crtTD > ros::Duration(0.0))
        //            {
        //                minTD = crtTD;
        //                idx_idc2iv_can = idx_idc_can;
        //            }
        //        }


        //        idc2ivTime.push_back(idc_tStmp.at(idc2iv_Idx));
        idc2iv_ptClouds.push_back(idc_ptClouds.at(idc2iv_Idx));
        idc2iv_Objlist_NO.push_back(idc_Objlist_NO.at(idc2iv_Idx));
        idc2iv_Objlist_YES.push_back(idc_Objlist_YES.at(idc2iv_Idx));
        //        idc2iv_can.push_back(idc_can.at(idx_idc2iv_can));

    }

    if (idc2iv_ptClouds.size() == 0 || idc2iv_Objlist_NO.size() == 0 || idc2iv_Objlist_YES.size() == 0 || v_can_sync2pc.size() == 0)
    {
        std::cout << "***Something is WRONG, probably innovusion time stamps are not fully covered by ibeo(.idc).***" << std::endl;
        exit(0);
    }

    for (int idx_all = 0; idx_all < iv_ptClouds.size(); idx_all++)
    {


        pcl::PointCloud<pcl::PointXYZ> pclcloud_iv_raw;
        pcl::PointCloud<pcl::PointXYZ> pclcloud_iv_t;

//        pclcloud_iv_t.width = iv_ptClouds.size();
//        pclcloud_iv_t.height = 1;
//        pclcloud_iv_t.is_dense = false;
//        pclcloud_iv_t.points.resize(pclcloud_iv_t.width * pclcloud_iv_t.height);

        sensor_msgs::PointCloud2 iv_pc;

        pcl::fromROSMsg (*iv_ptClouds.at(idx_all), pclcloud_iv_raw);
        pcl::fromROSMsg (*iv_ptClouds.at(idx_all), pclcloud_iv_t);

//        float theta = M_PI/2; // The angle of rotation in radians
        float tx;
        float ty;
        float tz;
        float yawrate_x;
        float yawrate_y;
        float yawrate_z;

        if (is_paras_given)
        {
            tx = paras.at(0);
            ty = paras.at(1);
            tz = paras.at(2);
            yawrate_x = paras.at(3);
            yawrate_y = paras.at(4);
            yawrate_z = paras.at(5);

        }

        else
        {
            tx = 1.7;
            ty = 0;
            tz = 1.46;
            yawrate_x = 0;
            yawrate_y = 0;
            yawrate_z = 0;
        }

        transform_coordinates t(tx, ty, tz, yawrate_x, yawrate_y, yawrate_z);

//        Eigen::Affine3f transform_mat = Eigen::Affine3f::Identity();
//        transform_mat.translation() << tx, ty, tz;
//        transform_mat.rotate (Eigen::AngleAxisf (theta, Eigen::Vector3f::UnitY()));
//        transform_mat.rotate (Eigen::AngleAxisf (theta*2, Eigen::Vector3f::UnitZ()));

        pcl::transformPointCloud(pclcloud_iv_raw, pclcloud_iv_t, t.get_transform_mat());

//        std::cout << transform_mat.matrix() << std::endl;
//        std::cout << t.get_transform_mat().matrix() << std::endl;


        pcl::toROSMsg(pclcloud_iv_t, iv_pc);

        write_bag.write("/iv_points", iv_tStmp.at(idx_all), iv_pc);
        write_bag.write("/lux_pointcloud", iv_tStmp.at(idx_all), *idc2iv_ptClouds.at(idx_all));
        write_bag.write("/lux_ObjectList_no", iv_tStmp.at(idx_all), *idc2iv_Objlist_NO.at(idx_all));
        write_bag.write("/lux_ObjectList_yes", iv_tStmp.at(idx_all), *idc2iv_Objlist_YES.at(idx_all));
        write_bag.write("/ld_imugps_can", iv_tStmp.at(idx_all), v_can_sync2pc.at(idx_all));
    }

//    for (int idx_lux_pc = 0; idx_lux_pc < idc_tStmp.size()-1; idx_lux_pc++)
//    {
//        write_bag.write("/lux_pointcloud", idc_tStmp.at(idx_lux_pc), *idc_ptClouds.at(idx_lux_pc));
//    }


    write_bag.close();
    std::cout << "Finish synchronizing process " << std::endl;
    /***************** Finish checking synchronization for pt cloud(idc&iv) and CanMsg with text files**********************/

    idc2ivTime.clear();
    idc2ivTime.shrink_to_fit();

    idc2iv_ptClouds.clear();
    idc2iv_ptClouds.shrink_to_fit();

    idc2iv_Objlist_NO.clear();
    idc2iv_Objlist_NO.shrink_to_fit();

    idc2iv_Objlist_YES.clear();
    idc2iv_Objlist_YES.shrink_to_fit();

    idc2iv_can.clear();
    idc2iv_can.shrink_to_fit();

    v_can_sync2pc.clear();
    v_can_sync2pc.shrink_to_fit();

//    vec_velocity_fq.clear();
//    vec_velocity_fq.shrink_to_fit();

//    vec_yawrate_fq.clear();
//    vec_yawrate_fq.shrink_to_fit();

//    vec_accx_fq.clear();
//    vec_accx_fq.shrink_to_fit();

//    vec_accy_fq.clear();
//    vec_accy_fq.shrink_to_fit();



    // idc_can.clear();
    // idc_can.shrink_to_fit();
    
    v_can_velocity.clear();
    v_can_velocity.shrink_to_fit();
    v_can_yawrate.clear();
    v_can_yawrate.shrink_to_fit();
    // idc_can_ts.clear();
    // idc_can_ts.shrink_to_fit();

    idc_tStmp.clear();
    idc_tStmp.shrink_to_fit();

    idc_ptClouds.clear();
    idc_ptClouds.shrink_to_fit();

    idc_Objlist_NO.clear();
    idc_Objlist_NO.shrink_to_fit();

    idc_Objlist_YES.clear();
    idc_Objlist_YES.shrink_to_fit();


    iv_tStmp.clear();
    iv_tStmp.shrink_to_fit();

    iv_ptClouds.clear();
    iv_ptClouds.shrink_to_fit();


    idc_topics.clear();
    idc_topics.shrink_to_fit();

    iv_topics.clear();
    iv_topics.shrink_to_fit();

    return true;
}
#endif // SYNC_IDC_H