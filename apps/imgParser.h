#ifndef IMGPARSER_H
#define IMGPARSER_H

// libs for rosbag API
#include <iostream>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
#include "ld_msgs/ld_image.h"

class imgParser
{
public:
    imgParser();
    void ImgRead(std::string img_bag, std::vector<ld_msgs::ld_image>& img_ros, ros::Time T1 , ros::Time T2);
    
private:
    // std::vector<sensor_msgs::PointCloud2> m_ivpc;
    std::vector<ros::Time> m_img_ts;

};

#endif // IMGPARSER_H