#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <dirent.h>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

class FileManager {
 public:
  FileManager();
  //    void GetAllFormatFiles(std::string path, std::vector<std::string>&
  //    files, std::string format,
  //                                             std::vector<std::string>&
  //                                             output_filenames,
  //                                             std::vector<std::string>&
  //                                             BAGfilenames);
  void GetAllFormatFiles(std::string path, std::vector<std::string>& files,
                         std::string format,
                         std::vector<std::string>& output_filenames,
                         std::vector<std::string>& BAGfilenames);
  void FileNamesSort(std::vector<std::string>& filenames);
  std::vector<std::string> GetAllBagFiles(std::string path, std::string format);
};

#endif  // FILEMANAGER_H
