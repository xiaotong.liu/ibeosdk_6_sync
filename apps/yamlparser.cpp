#include "yamlparser.h"

yamlParser::yamlParser() {
  YAML::Node cali = YAML::LoadFile("./config/cali.yaml");
  YAML::Node nodeT, nodeY,nodeC;
  nodeT = cali["Transportation Vector"];
  nodeY = cali["Yaw Pitch Roll"];
  nodeC = cali["Coordinate Preview"];
  transVector vec;
  vec.x = nodeT[0].as<double>();
  vec.y = nodeT[1].as<double>();
  vec.z = nodeT[2].as<double>();
  vec.yaw = nodeY[0].as<double>();
  vec.pitch = nodeY[1].as<double>();
  vec.roll = nodeY[2].as<double>();
  set_trans_vec(vec);
  coorsVector co;
  co.X = nodeC[0].as<double>();
  co.Y = nodeC[1].as<double>();
  co.Z = nodeC[2].as<double>();
  co.qw = nodeC[3].as<double>();
  co.qx = nodeC[4].as<double>();
  co.qy = nodeC[5].as<double>();
  co.qz = nodeC[6].as<double>();
  set_coor_vec(co);
}
