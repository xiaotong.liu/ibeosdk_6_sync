// Generated by gencpp from file ld_msgs/ld_gps.msg
// DO NOT EDIT!


#ifndef LD_MSGS_MESSAGE_LD_GPS_H
#define LD_MSGS_MESSAGE_LD_GPS_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/Vector3.h>

namespace ld_msgs
{
template <class ContainerAllocator>
struct ld_gps_
{
  typedef ld_gps_<ContainerAllocator> Type;

  ld_gps_()
    : header()
    , utctime()
    , timestamps(0.0)
    , linear_velocity()
    , linear_velocity_covariance()
    , altitude(0.0)
    , longitude(0.0)
    , latitude(0.0)  {
      linear_velocity_covariance.assign(0.0);
  }
  ld_gps_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , utctime(_alloc)
    , timestamps(0.0)
    , linear_velocity(_alloc)
    , linear_velocity_covariance()
    , altitude(0.0)
    , longitude(0.0)
    , latitude(0.0)  {
  (void)_alloc;
      linear_velocity_covariance.assign(0.0);
  }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _utctime_type;
  _utctime_type utctime;

   typedef double _timestamps_type;
  _timestamps_type timestamps;

   typedef  ::geometry_msgs::Vector3_<ContainerAllocator>  _linear_velocity_type;
  _linear_velocity_type linear_velocity;

   typedef boost::array<double, 9>  _linear_velocity_covariance_type;
  _linear_velocity_covariance_type linear_velocity_covariance;

   typedef double _altitude_type;
  _altitude_type altitude;

   typedef double _longitude_type;
  _longitude_type longitude;

   typedef double _latitude_type;
  _latitude_type latitude;





  typedef boost::shared_ptr< ::ld_msgs::ld_gps_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::ld_msgs::ld_gps_<ContainerAllocator> const> ConstPtr;

}; // struct ld_gps_

typedef ::ld_msgs::ld_gps_<std::allocator<void> > ld_gps;

typedef boost::shared_ptr< ::ld_msgs::ld_gps > ld_gpsPtr;
typedef boost::shared_ptr< ::ld_msgs::ld_gps const> ld_gpsConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::ld_msgs::ld_gps_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::ld_msgs::ld_gps_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace ld_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'ld_msgs': ['/home/lddev001/catkin_ws/src/ld_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::ld_msgs::ld_gps_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::ld_msgs::ld_gps_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ld_msgs::ld_gps_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ld_msgs::ld_gps_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ld_msgs::ld_gps_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ld_msgs::ld_gps_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::ld_msgs::ld_gps_<ContainerAllocator> >
{
  static const char* value()
  {
    return "73351f78d1d3f38a1a11bf17b733649c";
  }

  static const char* value(const ::ld_msgs::ld_gps_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x73351f78d1d3f38aULL;
  static const uint64_t static_value2 = 0x1a11bf17b733649cULL;
};

template<class ContainerAllocator>
struct DataType< ::ld_msgs::ld_gps_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ld_msgs/ld_gps";
  }

  static const char* value(const ::ld_msgs::ld_gps_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::ld_msgs::ld_gps_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
\n\
string utctime\n\
\n\
float64 timestamps\n\
\n\
geometry_msgs/Vector3 linear_velocity\n\
float64[9] linear_velocity_covariance\n\
\n\
float64 altitude\n\
\n\
float64 longitude\n\
\n\
float64 latitude\n\
\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Vector3\n\
# This represents a vector in free space. \n\
# It is only meant to represent a direction. Therefore, it does not\n\
# make sense to apply a translation to it (e.g., when applying a \n\
# generic rigid transformation to a Vector3, tf2 will only apply the\n\
# rotation). If you want your data to be translatable too, use the\n\
# geometry_msgs/Point message instead.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
";
  }

  static const char* value(const ::ld_msgs::ld_gps_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::ld_msgs::ld_gps_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.utctime);
      stream.next(m.timestamps);
      stream.next(m.linear_velocity);
      stream.next(m.linear_velocity_covariance);
      stream.next(m.altitude);
      stream.next(m.longitude);
      stream.next(m.latitude);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ld_gps_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::ld_msgs::ld_gps_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::ld_msgs::ld_gps_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "utctime: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.utctime);
    s << indent << "timestamps: ";
    Printer<double>::stream(s, indent + "  ", v.timestamps);
    s << indent << "linear_velocity: ";
    s << std::endl;
    Printer< ::geometry_msgs::Vector3_<ContainerAllocator> >::stream(s, indent + "  ", v.linear_velocity);
    s << indent << "linear_velocity_covariance[]" << std::endl;
    for (size_t i = 0; i < v.linear_velocity_covariance.size(); ++i)
    {
      s << indent << "  linear_velocity_covariance[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.linear_velocity_covariance[i]);
    }
    s << indent << "altitude: ";
    Printer<double>::stream(s, indent + "  ", v.altitude);
    s << indent << "longitude: ";
    Printer<double>::stream(s, indent + "  ", v.longitude);
    s << indent << "latitude: ";
    Printer<double>::stream(s, indent + "  ", v.latitude);
  }
};

} // namespace message_operations
} // namespace ros

#endif // LD_MSGS_MESSAGE_LD_GPS_H
