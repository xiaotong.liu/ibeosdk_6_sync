// Generated by gencpp from file ld_msgs/ld_image.msg
// DO NOT EDIT!


#ifndef LD_MSGS_MESSAGE_LD_IMAGE_H
#define LD_MSGS_MESSAGE_LD_IMAGE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace ld_msgs
{
template <class ContainerAllocator>
struct ld_image_
{
  typedef ld_image_<ContainerAllocator> Type;

  ld_image_()
    : header()
    , image_format(0)
    , device_id(0)
    , image_width(0)
    , image_height(0)
    , is_bigendian(0)
    , compressed_size(0)
    , compressed_data()  {
    }
  ld_image_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , image_format(0)
    , device_id(0)
    , image_width(0)
    , image_height(0)
    , is_bigendian(0)
    , compressed_size(0)
    , compressed_data(_alloc)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef uint8_t _image_format_type;
  _image_format_type image_format;

   typedef uint8_t _device_id_type;
  _device_id_type device_id;

   typedef uint32_t _image_width_type;
  _image_width_type image_width;

   typedef uint32_t _image_height_type;
  _image_height_type image_height;

   typedef uint8_t _is_bigendian_type;
  _is_bigendian_type is_bigendian;

   typedef uint32_t _compressed_size_type;
  _compressed_size_type compressed_size;

   typedef std::vector<uint8_t, typename ContainerAllocator::template rebind<uint8_t>::other >  _compressed_data_type;
  _compressed_data_type compressed_data;





  typedef boost::shared_ptr< ::ld_msgs::ld_image_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::ld_msgs::ld_image_<ContainerAllocator> const> ConstPtr;

}; // struct ld_image_

typedef ::ld_msgs::ld_image_<std::allocator<void> > ld_image;

typedef boost::shared_ptr< ::ld_msgs::ld_image > ld_imagePtr;
typedef boost::shared_ptr< ::ld_msgs::ld_image const> ld_imageConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::ld_msgs::ld_image_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::ld_msgs::ld_image_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace ld_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'ld_msgs': ['/home/chunming/software/canmessageparser/src/ld_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::ld_msgs::ld_image_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::ld_msgs::ld_image_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ld_msgs::ld_image_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ld_msgs::ld_image_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ld_msgs::ld_image_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ld_msgs::ld_image_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::ld_msgs::ld_image_<ContainerAllocator> >
{
  static const char* value()
  {
    return "53b8cf92aa89b6b46491c17a47dbdac8";
  }

  static const char* value(const ::ld_msgs::ld_image_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x53b8cf92aa89b6b4ULL;
  static const uint64_t static_value2 = 0x6491c17a47dbdac8ULL;
};

template<class ContainerAllocator>
struct DataType< ::ld_msgs::ld_image_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ld_msgs/ld_image";
  }

  static const char* value(const ::ld_msgs::ld_image_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::ld_msgs::ld_image_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# This message contains an compressed image\n\
# the compressed data contains the data struct header and compressed data\n\
# \n\
\n\
Header header              # header timestamp shoul be acquisition time of the image\n\
\n\
uint8 image_format         # format of the image: 0-JPEG 1-MJPEG 2-GRAY8 3-YUV420 4-YUV422\n\
\n\
uint8 device_id            # ID for the camera device\n\
\n\
\n\
uint32 image_width         # image width, that is, number of columns\n\
\n\
uint32 image_height        # image height, that is, number of rows\n\
\n\
\n\
uint8 is_bigendian         # is this data bigendian?\n\
\n\
uint32 compressed_size     # the size of the following compressed data\n\
\n\
uint8[] compressed_data    # the compressed image data, including the data header and compressed data \n\
\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::ld_msgs::ld_image_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::ld_msgs::ld_image_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.image_format);
      stream.next(m.device_id);
      stream.next(m.image_width);
      stream.next(m.image_height);
      stream.next(m.is_bigendian);
      stream.next(m.compressed_size);
      stream.next(m.compressed_data);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ld_image_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::ld_msgs::ld_image_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::ld_msgs::ld_image_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "image_format: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.image_format);
    s << indent << "device_id: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.device_id);
    s << indent << "image_width: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.image_width);
    s << indent << "image_height: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.image_height);
    s << indent << "is_bigendian: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.is_bigendian);
    s << indent << "compressed_size: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.compressed_size);
    s << indent << "compressed_data[]" << std::endl;
    for (size_t i = 0; i < v.compressed_data.size(); ++i)
    {
      s << indent << "  compressed_data[" << i << "]: ";
      Printer<uint8_t>::stream(s, indent + "  ", v.compressed_data[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // LD_MSGS_MESSAGE_LD_IMAGE_H
