#ifndef LD_IVPOINT_PCL_H_
#define LD_IVPOINT_PCL_H_

#define PCL_NO_PRECOMPILE

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/extract_indices.h>

namespace ld{

struct ivStruct{
  PCL_ADD_POINT4D; // include XYZ coordinates
  double timestamp; // timestamp of the point
  unsigned char intensity; // intenstiy of the point
  unsigned char flags; // flags of the point
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW; // make sure our new allocators are aligned
} EIGEN_ALIGN16;

}

POINT_CLOUD_REGISTER_POINT_STRUCT(ld::ivStruct,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (double, timestamp, timestamp)
                                  (unsigned char, intensity, intensity)
                                  (unsigned char, flags, flags)
)

#endif // LD_IVPOINT_PCL_H
