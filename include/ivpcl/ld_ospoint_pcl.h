#ifndef LD_OSPOINT_PCL_H_
#define LD_OSPOINT_PCL_H_

#define PCL_NO_PRECOMPILE

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/extract_indices.h>

namespace ld{
struct osStruct {
    PCL_ADD_POINT4D;  // 添加pcl里xyz+padding
    float intensity;
    uint32_t t;
    uint16_t reflectivity;
    uint8_t ring;
    uint16_t noise;  // 28-31
    uint32_t range;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;
}

POINT_CLOUD_REGISTER_POINT_STRUCT(ld::osStruct,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (uint32_t, t, t)
                                  (uint16_t, reflectivity, reflectivity)
                                  (uint8_t, ring, ring)
                                  (uint16_t, noise,noise)
                                  (uint32_t, range, range)
)
#endif // LD_OSPOINT_PCL_H
