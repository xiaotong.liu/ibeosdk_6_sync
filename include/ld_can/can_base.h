/**
 * @file can_base.h
 * @author cai, chunming (chunming.cai@liangdao.de)
 * @brief
 * @version 0.1
 * @date 2020-04-01
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef LD_CAN_SRC_CAN_BASE_H_
#define LD_CAN_SRC_CAN_BASE_H_

namespace ld {

#include <iostream>
#include <stdint.h>
#include <string>

/**
 * @brief
 *
 */
template <typename T> class CanMsgBase {
public: //  Constructor and Deconstructor
  CanMsgBase();
  ~CanMsgBase() {}

public: // Getter
  uint32_t get_id() const { return id_; }
  T get_factor() const { return factor_; }
  T get_offset() const { return offset_; }
  uint8_t get_lsb() const { return lsb_; }
  uint8_t get_msb() const { return msb_; }
  uint8_t get_sign_bit() const { return signb_; }
  bool get_signbit_available() const { return signb_available_; }
  bool get_is_big_endian() const { return is_big_endian_; }
  T get_unit_trans() const { return unit_trans_; }
  std::string get_name() const { return name_; }
  uint8_t get_len() const { return len_; }

public: // Setter
  void set_id(const uint32_t &id) { id_ = id; }
  void set_factor(const T &factor) { factor_ = factor; }
  void set_offset(const T &offset) { offset_ = offset; }
  void set_lsb(const uint8_t &lsb) { lsb_ = lsb; }
  void set_msb(const uint8_t &msb) { msb_ = msb; }
  void set_sign_bit(const uint8_t &signb) { signb_ = signb; }
  void set_signbit_available(const bool &signb_available) {
    signb_available_ = signb_available;
  }
  void set_is_big_endian(const bool &is_big_endian) {
    is_big_endian_ = is_big_endian;
  }
  void set_unit_trans(const T &unit_trans) { unit_trans_ = unit_trans; }

  void set_name(const std::string &name) { name_ = name; }

public:
  uint32_t id_;   // Message ID
  T factor_;      // factor of the message
  T offset_;      // offset of the messate
  uint8_t lsb_;   // the least significant bit
  uint8_t msb_;   // the most significant bit
  uint8_t signb_; // the bit for sign
  bool signb_available_{
      false}; // flag indicats if there is a sign bit for the message
  bool is_big_endian_{false}; // byte order
  T unit_trans_{1};           // unit
  std::string name_;
  bool two_compoment_{false};
  uint8_t len_;
};

template <typename T>
CanMsgBase<T>::CanMsgBase()
    : id_(0), factor_(1), offset_(0), lsb_(0), msb_(0), signb_(0),
      signb_available_(false), is_big_endian_(false), unit_trans_(1) {}

} // namespace ld

#endif // LD_CAN_SRC_CAN_BASE_H_
