/**
 * @file can_interpreter.h
 * @author cai, chunming (chunming.cai@liangdao.de)
 * @brief can interpreter, just work for Linux system
 * @version 0.1
 * @date 2020-04-01
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef LD_CAN_CAN_INTERPRETER_H_
#define LD_CAN_CAN_INTERPRETER_H_
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <string>
// #include <cstdint>
#include <stdio.h>

#include "ld_can/can_base.h"

// #define DebugCode

namespace ld {

enum CanMsgType { velocity, accx, accy, ld_can };

/**
 * @brief Template class CanInterpreter for interpreting a can message
 *
 * @tparam T
 */
template <typename T> class CanInterpreter {
public: // Constructor and Decontstructor
  CanInterpreter();
  ~CanInterpreter();

private:
  CanMsgBase<T> can_m_; // object to hold the data parsed from can yaml

private:
  uint64_t *data_;  // point to joint data from the 8 bytes can data
  CanMsgType type_; // can message type
  T ret_val_;       // return value, togeter with the sign
  int sign_{1};     // value sign
  uint64_t sign_mask_{1};
  uint8_t lbit_{
      0}; // how many bit should the data move to links when interpreting
          // the data
  uint8_t rbit_{0}; // how many bit should the data move to rechts when
  // interpreting the data based on the to links moved data, it
  // means, if the data moved 6 bits to links, it should move
  // right 6 bits and then move more bit to right for the real
  // results
  uint64_t value_;

public: // Getter
  int get_sign() const { return sign_; }
  std::string get_name() const { return can_m_.name_; }

public: // Setter
  void set_id(const uint32_t &id) { can_m_.id_ = id; }
  void set_factor(const T &factor) { can_m_.factor_ = factor; }
  void set_offset(const T &offset) { can_m_.offset_ = offset; }
  void set_lsb(const uint8_t &lsb) { can_m_.lsb_ = lsb; }
  void set_msb(const uint8_t &msb) { can_m_.msb_ = msb; }
  void set_sign_bit(const uint8_t &signb) {
    // std::cout << "sign bit is: " << (unsigned int)signb << std::endl;
    can_m_.signb_ = signb;

    sign_mask_ = sign_mask_ << can_m_.signb_;
  }
  void set_signbit_available(const bool &signb_available) {
    can_m_.signb_available_ = signb_available;
  }
  void set_is_big_endian(const bool &is_big_endian) {
    can_m_.is_big_endian_ = is_big_endian;
  }
  void set_unit_trans(const T &unit_trans) { can_m_.unit_trans_ = unit_trans; }

  void set_name(const std::string &name) { can_m_.name_ = name; }
  void set_two_complement(const bool &two_com) {
    can_m_.two_compoment_ = two_com;
  }

public:
  /**
   * @brief function to interpret the can message
   *
   * @param data pointer to the data array
   * @param len can data array length, usually 8, but not all 8
   * @return T
   */
  T can_parser(const uint8_t *data, const uint8_t &len);

private:
  /**
   * @brief function to copy 8 bytes to one 64bit data(uint64_t)
   *
   * @param data
   * @param len
   */
  void data_joint(const uint8_t *data, const uint8_t &len);
  /**
   * @brief funciton to correct the data order, if the data is a big endian
   * data, the function can correct the data orders
   *
   */
  void byteorder(const uint8_t &len);
};

template <typename T> CanInterpreter<T>::CanInterpreter() {
  type_ = ld_can;
  ret_val_ = 0;
  data_ = static_cast<uint64_t *>(malloc(sizeof(uint64_t)));
  memset(data_, 0, sizeof(uint64_t));
}

template <typename T> CanInterpreter<T>::~CanInterpreter() { free(data_); }

template <typename T>
T CanInterpreter<T>::can_parser(const uint8_t *data, const uint8_t &len) {

  uint8_t i = len;

// printf("The data is: ");
// while (i--)
//   printf("%d ", data[i]);
// printf("\n");

#ifdef DebugCode
  std::cout << *data_ << std::endl;
#endif

  data_joint(data, len);

#ifdef DebugCode

  std::cout << "joint data is: " << *data_ << std::endl;

#endif

  // std::cout << "Before:" << (unsigned long)*data_ << std::endl;

  if (can_m_.get_signbit_available()) {
    // printf("Get the sign\n");
    if ((sign_mask_ & *data_) == 0) {

      // std::cout << "sign_mask is: " << sign_mask_ << std::endl;
      sign_ = 1;
    }

    else {
      sign_ = -1;
    }

    // std::cout << "sign is***: " << sign_ << std::endl;
  }

  if (can_m_.get_is_big_endian()) {
    // printf("byter orderner exectued.\n");
    byteorder(len);
  }

#ifdef DebugCode
  std::cout << "datta after byteorder " << *data_ << std::endl;
#endif

  // i = len;
  // while (i--)
  //   printf("%d ", data[i]);

  // printf("\n");

  // std::cout << "After: " << (unsigned long)*data_ << std::endl;

  if (can_m_.get_is_big_endian()) {

#ifdef DebugCode
    printf("Process data with big endian.\n");
#endif

    // printf("Process data with big endian.\n");
    lbit_ = (can_m_.get_msb() / 8) * 8 + 7 - can_m_.get_msb() % 8;
    rbit_ = (len - 1) * 8 - (can_m_.get_lsb() / 8) * 8 + can_m_.get_lsb() % 8 +
            lbit_;
    // printf("Left move: %d\n", lbit_);
    // printf("Right move: %d\n", rbit_);
    if (can_m_.two_compoment_) {

#ifdef DebugCode
      printf("Process data with big endian and two completent.\n");
#endif

      value_ = (*data_ << lbit_) >> rbit_;

#ifdef DebugCode
      std::cout << "lbit is: " << (int)(lbit_) << " rbit is: " << (int)(rbit_)
                << " "
                << "Value is: " << value_ << std::endl;
#endif

      uint8_t bitnum = can_m_.get_msb() % 8 - can_m_.get_lsb() % 8 +
                       8 * (can_m_.get_lsb() / 8 - can_m_.get_msb() / 8);
#ifdef DebugCode
      std::cout << "bit number is:  " << (int)bitnum << std::endl;
#endif
      uint64_t mask_msb = 1;
      if (((mask_msb << bitnum) & value_) > 0) {
#ifdef DebugCode
        std::cout << "Enter negative calculation " << std::endl;
#endif
        ret_val_ = (mask_msb << (bitnum + 1)) - value_;
        ret_val_ = -1 * ret_val_;

      } else {
        ret_val_ = (*data_ << lbit_) >> rbit_;
      }
    } else {
      ret_val_ = (*data_ << lbit_) >> rbit_;
    }

    // ret_val_ = (*data_ << lbit_) >> rbit_;
    //  printf("raw data is: %d\n", (int)ret_val_);
  } else {
    // printf("Process data with little endian.\n");
    lbit_ = 63 - can_m_.get_msb();
    rbit_ = lbit_ + can_m_.get_lsb();

#ifdef DebugCode
    printf("lsb is: %u\n", can_m_.get_lsb());
    printf("msb is: %u\n", can_m_.get_msb());
    printf("Left move: %d\n", lbit_);
    printf("Right move: %d\n", rbit_);
#endif

    if (can_m_.two_compoment_) {

#ifdef DebugCode
      printf("Enter two compoment\n");
#endif

      value_ = (*data_ << lbit_) >> rbit_;

#ifdef DebugCode
      std::cout << *data_ << std::endl;
#endif

      uint8_t bitnum = can_m_.get_msb() - can_m_.get_lsb();
      uint64_t mask_msb = 1;
      if (((mask_msb << bitnum) & value_) > 0) {

        ret_val_ = (mask_msb << (bitnum + 1)) - value_;

        ret_val_ = -1 * ret_val_;

#ifdef DebugCode
        std::cout << value_ << " " << (mask_msb << (bitnum + 1))
                  << " enter mask code " << ret_val_ << std::endl;
#endif
      } else {
        ret_val_ = (*data_ << lbit_) >> rbit_;
      }

    } else {
      ret_val_ = (*data_ << lbit_) >> rbit_;
    }

    // ret_val_ = (*data_ << lbit_) >> rbit_;

    // std::cout << "raw value is: " << std::fixed << ret_val_ << std::endl;
  }

#ifdef DebugCode
  std::cout << std::fixed << can_m_.factor_ << std::endl;
#endif

  ret_val_ = (ret_val_ * can_m_.get_factor() + can_m_.get_offset()) *
             can_m_.get_unit_trans() * sign_;

  return ret_val_;
}

template <typename T>
void CanInterpreter<T>::data_joint(const uint8_t *data, const uint8_t &len) {
  std::memcpy(data_, data, len);
}

template <typename T> void CanInterpreter<T>::byteorder(const uint8_t &len) {
  uint8_t buf[sizeof(uint64_t)];
  uint8_t n = sizeof(uint64_t);
  uint8_t n_len = len;
  uint8_t *dataAddress = reinterpret_cast<uint8_t *>(data_);
  memcpy(buf, data_, n);

  uint8_t *bufEndAddress = buf + n_len - 1;

  while (n_len--) {
    *dataAddress++ = *bufEndAddress--;
  }
}

} // namespace ld

#endif // LD_CAN_CAN_INTERPRETER_H_
