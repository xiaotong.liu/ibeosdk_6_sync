#!/bin/bash
repo_root_path=".."
install_dir=../install
cd ${repo_root_path}
if [ -d "builds" ]; then
    rm -rf builds
fi
mkdir -p builds
cd builds
cmake -DCMAKE_BUILD_TYPE=Debug -DCompileTest=OFF -DCMAKE_INSTALL_PREFIX=$install_dir ..
cmake --build . --config Debug
cmake --install . --config Debug

