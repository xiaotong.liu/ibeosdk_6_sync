#!/bin/bash
set -eu
root="../../apps/"
lint_dir=("${root}")

for d in "${lint_dir[*]}"
do
cpplint --recursive $d
done
