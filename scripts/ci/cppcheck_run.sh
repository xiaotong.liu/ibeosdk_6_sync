#!/bin/bash
set -eu
root="../.."
check_dir=("${root}")
# - cppcheck --enable=style --error-exitcode=1 -i$CI_PROJECT_DIR/ld_data/3rd_part $CI_PROJECT_DIR/ld_data
        # - cppcheck --enable=style --error-exitcode=1 -i$CI_PROJECT_DIR/ld_data/3rd_part $CI_PROJECT_DIR/tests
for d in "${check_dir[*]}"
do
cppcheck --enable=style --error-exitcode=1 $d
done
