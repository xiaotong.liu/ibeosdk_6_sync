#!/bin/bash
git config core.filemode false
root="../.."
format_dir=("${root}/ld_data/basic" "${root}/ld_data/frame" "${root}/ld_data/misc" "${root}/test_tools" "${root}/tests")

for d in "${format_dir[*]}"
do
find $d -iname *.h -o -iname *.c -o -iname *.cpp -o -iname *.hpp \
    | xargs clang-format -style=file -i -fallback-style=none
done

git diff --ignore-space-at-eol > clang_format.patch

# cat ./clang_format.patch
# Delete if 0 size
if [ ! -s clang_format.patch ]
then
    rm clang_format.patch
    echo "format is no error."
    exit 0
else
    echo "format error:"
    cat ./clang_format.patch
    exit 1
fi