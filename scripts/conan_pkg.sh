#!/bin/bash
# set -e  # exit on error.

virtual_env=conan
USAGE=$(cat <<EOF
USAGE: $0 options.
  options:
    '--vir_env/-v value':  set conan virtual environment to the value, default is 'conan'.
    '--force/-f':          overwrite exist package.
    '--help/-h':           print this message and exit.\n
EOF
)
while (( "$#" )); do
    if [ $1 = "--vir_env" ] || [ $1 = "-v" ]; then
        virtual_env=$2
        shift
    elif [ $1 = "--force" ] || [ $1 = "-f" ]; then
        force=1
    elif [ $1 = "--help" ] || [ $1 = "-h" ]; then
        printf "$USAGE"
        exit 0
    else
        printf "unknown options: '$1'.\n"
        printf "$USAGE"
        exit 1
    fi
    shift
done

pro_root=".."
cd $pro_root

# build and install
if [ -d "conan_builds" ]; then
    rm -r conan_builds
fi
mkdir -p conan_builds
cd conan_builds
source `which virtualenvwrapper.sh`
workon $virtual_env
conan install ..
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$pro_root/conan_install ..
cmake --build . --config Release
ctest -VV
if [ $? -ne 0 ]; then
    echo "ctest failed."
    exit
fi
cmake --install . --config Release

# package
cd $pro_root/conan_pkg
if [ -n "$force" ]; then
    conan export-pkg . yabin/dev --package-folder=$pro_root/conan_install -f
else
    conan export-pkg . yabin/dev --package-folder=$pro_root/conan_install
fi
if [ $? -ne 0 ]; then
    echo "conan package failed."
    exit
fi
# test
cd $pro_root/conan_pkg/pkg_test
if [ -d "builds" ]; then
    rm -r builds
fi
mkdir -p builds
cd builds
conan install ..
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release
./bin/main
if [ $? -ne 0 ]; then
    echo "test pkg failed."
    exit
fi
